<div class="container">
    <?php $link = Session::get('permession'); ?>
    <div class="col-xs-6">
        <h3>School list</h3>
        <hr/>
        <div id="schoolToolbar">
            <a href="<?php echo URL . $link['Learn Service']['Create School']; ?>" type="button" class="btn btn-default">Add New School</a>
        </div>
        <table class="table" id="adminTable" data-toggle="table" data-url="<?php echo URL . "/org/schoolData/" . $this->orgId; ?>" 
               data-cache="false"
               data-height="500" 
               data-click-to-select="true"
               data-toolbar="#schoolToolbar"
               data-show-refresh="true" 
               data-show-columns="true" 
               data-search="true"
               >
            <thead>
                <tr>
                    <th data-field="id">#</th>
                    <th data-field="name">Name</th>
                    <th data-field="address" data-visible="false">Address</th>
                    <th data-field="suburb" data-visible="false">Suburb</th>
                    <th data-field="city" data-visible="false">City</th>
                    <th data-field="full_address" data-visible="false">Full address</th>
                    <th data-field="introduction">Introduction</th>
                    <th data-field="isActive"  data-formatter="Active">Stats</th>     
                    <th data-field="" data-formatter="ViewSchool"></th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="col-xs-6">
        <h3>Manager list</h3>
        <hr/>
        <div id="managerToolbar">
            <a href="<?php echo URL . $link['Learn Service']['Create Manager']; ?>" type="button" class="btn btn-default">Add New Manager</a>
        </div>
        
        <table class="table" id="table" data-toggle="table" data-url="<?php echo URL . "/org/managerData/" . $this->orgId; ?>" 
               data-cache="false"
               data-height="500" 
               data-click-to-select="true"
               data-toolbar="#managerToolbar"
               data-show-refresh="true" 
               data-show-columns="true" 
               data-search="true"
               >
            <thead>
                <tr>
                    <th data-field="id">#</th>
                    <th data-field="full_name">Full Name</th>
                    <th data-field="isActive"  data-formatter="Active">Stats</th>     
                    <th data-field="" data-formatter="ViewManager"></th>
                </tr>
            </thead>
        </table>
    </div>

    <script>
        function ViewSchool(value, row, index) {
            var link = "<?php echo URL . $link['Learn Service']['View School'] . "view/"; ?>" + row.id;
            return ['<a class="btn" href="' + link + '" title="view">View</a>'].join('');
        }
        function ViewManager(value, row, index) {
            var link = "<?php echo URL . $link['Learn Service']['View Manager'] . "view/"; ?>" + row.id;
            return ['<a class="btn" href="' + link + '" title="view">View</a>'].join('');
        }
        function Active(value, row, index) {
            if (value === "1")
            {
                return ['<span class="label label-success">Active</span>'].join('');
            } else {
                return ['<span class="label label-default">No Active</span>'].join('');
            }
        }
    </script>
</div>
