<div class="container">
    <form class="form-horizontal" method="post" action="<?php echo URL; ?>org/newOrg">
        <fieldset>
            <!-- Form Name -->
            <legend>New Org</legend>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="name">Org Name</label>  
                <div class="col-md-4">
                    <input id="name" name="name" type="text" placeholder="name" class="form-control input-md" required="" data-validate="orgAvailable">
                </div>
            </div>

            <!-- Submit button (inline) -->
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-4"> 
                        <input class="btn btn-primary" name="submitOrgName" type="submit" value="Next Step"/>
                    </div>
                </div>
        </fieldset>
    </form>
    <script>
        $.verify.addRules({
            orgAvailable: {
                fn: function (r) {
                    var n = r.val();
                    var text = orgValidator(n);
                    if (!text)
                    {
                        return "Please type other org name!";
                    } else {
                        return true;
                    }
                }
            }
        });

        function orgValidator(n) {
            var isSuccess = false;
            $.ajax({
                url: "<?php echo URL ?>org/orgAvailable",
                type: "post",
                data: {"name": n},
                async: false,
                success:
                        function (msg) {
                            isSuccess = msg == "available" ? true : false
                        }
            });
            return isSuccess;
        }
    </script>
</div>
