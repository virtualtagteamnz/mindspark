<div class="container">
    <div class="col-xs-10 col-xs-offset-1">
        <form class="form-horizontal" method="post" action="<?php echo URL ?>org/orgDetailForm/<?php echo $this->orgDetail->site_contact_id; ?>">
            <fieldset>
                <!-- Form Name -->
                <div class="col-xs-12">
                    <legend>Org#: <?php echo $this->orgDetail->orgId; ?>
                        <div class="col-xs-2 pull-right">
                            <input type="checkbox" id="isActive" name="isActive" data-toggle="toggle" data-on="Enabled" data-off="Disabled"data-size="mini" data-onstyle="success" data-offstyle="danger">
                        </div>
                    </legend>
                </div>
                <input name="id" type="hidden" value="<?php echo $this->orgDetail->orgId; ?>"/>
                <input name="site_contact_id" type="hidden" value="<?php echo $this->orgDetail->site_contact_id; ?>"/>
                <!--left part-->
                <div class="col-xs-6">
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-xs-4 control-label" for="name">Org Name</label>  
                        <div class="col-xs-8">
                            <input id="name" name="name" type="text" value="<?php echo $this->orgDetail->name ?>" class="form-control input-md" required="" readonly>

                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-xs-4 control-label" for="landline">Landline</label>  
                        <div class="col-xs-8">
                            <input id="landline" name="landline" type="text" placeholder="landline" class="form-control input-md">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-xs-4 control-label" for="mobile">Phone</label>  
                        <div class="col-xs-8">
                            <input id="mobile" name="mobile" type="text" placeholder="phone" class="form-control input-md">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-xs-4 control-label" for="fax">Fax</label>  
                        <div class="col-xs-8">
                            <input id="fax" name="fax" type="text" placeholder="fax" class="form-control input-md">
                        </div>
                    </div>


                </div><!-- left part end -->

                <!--right part-->
                <div class="col-xs-6">
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-xs-4 control-label" for="textinput">Full Address</label>  
                        <div class="col-xs-8">
                            <input id="autocomplete" name="full_address" type="text" placeholder="Enter your address" class="form-control input-md" data-validate="required">
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-xs-4 control-label" for="textinput">Street address</label>
                        <div class="col-xs-3">
                            <input id="street_number" name="street_number" type="text" placeholder="number" class="form-control input-md" required="" readonly>
                        </div>
                        <div class="col-xs-5">
                            <input id="route" name="address" type="text" placeholder="street" class="form-control input-md" required="" readonly>
                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-xs-4 control-label" for="suburb">Suburb</label>  
                        <div class="col-xs-8">
                            <input id="administrative_area_level_1" name="suburb" type="text" placeholder="suburb" class="form-control input-md" readonly>

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-xs-4 control-label" for="suburb">City</label>  
                        <div class="col-xs-4">
                            <input id="locality" name="city" type="text" placeholder="city" class="form-control input-md" required="" readonly>
                        </div>
                        <div class="col-xs-4">
                            <input id="postal_code" name="postcode" type="text" placeholder="post code" class="form-control input-md">
                        </div>
                    </div>


                </div><!--right part end-->

                <div class="col-xs-12">
                    <!-- Textarea -->
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="introduction">Introduction</label>
                        <div class="col-xs-10">                     
                            <textarea class="form-control" id="introduction" name="introduction"></textarea>
                            <span class="help-block">max 200 letters</span>  
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <!-- Textarea -->
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="description">Description</label>
                        <div class="col-xs-10">                     
                            <textarea class="form-control" id="description" name="description"></textarea>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12">
                    <!-- Textarea -->
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="submitOrgDetail"></label>
                        <div class="col-xs-10">                     
                            <input name="submitOrgDetail" class="btn btn-primary" type="submit" value="Update Org"/>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
    <script>
        $(document).ready(function () {
            var isActive = <?php echo $this->orgDetail->isActive; ?>;
            if (isActive)
            {
                $('#isActive').bootstrapToggle('on');
            } else {
                $('#isActive').bootstrapToggle('off');
            }

            $('#description').summernote(
                    {
                        height: 300, // set editor height
                        minHeight: 50, // set minimum height of editor
                        maxHeight: 800, // set maximum height of editor
                        styleWithSpan: false,
                        toolbar: [
                            ['style', ['style']],
                            ['font', ['bold', 'italic', 'underline', 'clear']],
                            ['fontname', ['fontname']],
                            ['color', ['color']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['height', ['height']],
                            ['table', ['table']],
                            ['insert', ['link', 'hr']],
                            ['view', ['fullscreen', 'codeview']],
                            ['help', ['help']]
                        ]
                    });
        });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdAcc2hvfqZKLctapVS7DLiPNQCATwKjU&signed_in=true&libraries=places&callback=initAutocomplete"></script>

</div>