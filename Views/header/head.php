<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <!--general css-->
        <link href="<?php echo URL . 'public/css/bootstrap.css'; ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo URL . 'public/css/font-awesome.min.css'; ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php // echo URL . 'public/css/kendo.common-bootstrap.min.css'; ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php // echo URL . 'public/css/kendo.bootstrap.min.css'; ?>" rel="stylesheet" type="text/css"/>
        <!--optional general css-->
        <?php if (isset($this->publicCSS)): ?>
            <?php foreach ($this->publicCSS as $css): ?>
                <link href="<?php echo URL . 'public/css/' . $css; ?>" rel="stylesheet" type="text/css"/>
            <?php endforeach; ?>
        <?php endif; ?>
        <!--customized css-->
        <link href="<?php echo URL . 'public/css/style.css'; ?>" rel="stylesheet" type="text/css"/>
        <!--specific css-->
        <?php if (isset($this->css)): ?>
            <?php foreach ($this->css as $css): ?>
                <link href="<?php echo URL . 'Views/' . $css; ?>" rel="stylesheet" type="text/css"/>
            <?php endforeach; ?>
        <?php endif; ?>
        <!--general js-->
        <script src="<?php echo URL; ?>public/js/jquery-2.1.4.min.js" type="text/javascript"></script>
        <script src="<?php echo URL; ?>public/js/bootstrap.js" type="text/javascript"></script>
        <!--<script src="<?php echo URL; ?>public/js/kendo.all.min.js" type="text/javascript"></script>-->
        <script src="<?php echo URL; ?>public/js/verify.min.js" type="text/javascript"></script>
        <!--optional general js-->
        <?php if (isset($this->publicJS)): ?>
            <?php foreach ($this->publicJS as $js): ?>
                <script src="<?php echo URL . 'public/js/' . $js; ?>" type="text/javascript"></script>
            <?php endforeach; ?>
        <?php endif; ?>
        <!--customized js-->
         <script src="<?php echo URL; ?>public/js/main.js" type="text/javascript"></script>       
        <!--specific js-->
        <?php if (isset($this->js)): ?>
            <?php foreach ($this->js as $js): ?>
                <script src="<?php echo URL . 'Views/' . $js; ?>" type="text/javascript"></script>
            <?php endforeach; ?>
        <?php endif; ?>
        <!--page title-->
        <title><?php echo $this->title; ?></title>
    </head>
    <body>