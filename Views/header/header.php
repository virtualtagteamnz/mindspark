<header>
    <div class="header-logo text-center">
        <img src="<?php echo URL; ?>/public/images/logo.png">
    </div>
    <div class="header-nav">
        <div class="container">
            <ul class="list-inline pull-left">
                <li ><a href="<?php echo URL; ?>dashboard">Dashboard</a></li>
                <?php if (Session::get('role') == "Superadmin"): ?>
                    <?php
                    $super = Session::get('permession');
                    $test = $super['Learn Service'];
                    $super = $super['Superadmin'];
                    
                    ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Super Admin<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?php foreach ($super as $menu_item => $link): ?>
                                <li><a href="<?php echo URL . $link; ?>"><?php echo $menu_item; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                <?php endif; ?>
                <li ><a href="<?php echo URL; ?>LearnService">Learn Service</a></li>
                <?php // foreach (Session::get('permession') as $menus => $menu): ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Test<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?php foreach ($test as $menu_item => $link): ?>
                                <li><a href="<?php echo URL . $link; ?>"><?php echo $menu_item; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                <?php // endforeach; ?>
            </ul>
            <ul class="list-inline pull-right">
                <li ><a href="<?php echo URL; ?>account/logout">Logout</a></li>
            </ul>
        </div>

    </div>
</header>