<div class="container login-container">
    <div class="col-xs-12">
        <div class="col-xs-6 text-center">
            <img src="../public/images/logo.png">
        </div>
        <div class="col-xs-6">
            <form class="form-horizontal" method="post" action="<?php echo URL; ?>account/login">
                <fieldset>
                    <!-- Form Name -->
                    <legend>Login</legend>
                    <div class="form-group">
                        <div class="col-md-12 text-center login-message">
                            <h4><?php echo $this->msg; ?></h4>
                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="username">Username</label>  
                        <div class="col-md-4">
                            <input id="username" name="username" type="text" placeholder="super" class="form-control input-md" data-validate="required,min(4),max(30)">
                        </div>
                    </div>
                    <!-- Password input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="password">Password</label>
                        <div class="col-md-4">
                            <input id="password" name="password" type="password" placeholder="123123" class="form-control input-md" data-validate="required,min(6),max(30)">
                        </div>
                    </div>
                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="login"></label>
                        <div class="col-md-4">
                            <button id="login" name="login" class="btn btn-default">Login</button>
                        </div>
                    </div>

                </fieldset>
            </form>
            <script>
                <?php echo $this->scr; ?>
            </script>
        </div>
    </div>
</div>
