
<div class="container">
    <div class="col-xs-10 col-xs-offset-1">
        <form class="form-horizontal" method="post" action="<?php echo URL ?>admin/adminDetailForm/<?php echo $this->adminDetail->id ?>">
            <fieldset>
                <!-- Form Name -->
                <div class="col-xs-12">
                    <legend>Admin#: <?php echo $this->adminDetail->adminId; ?>
                        <div class="col-xs-2 pull-right">
                            <input type="checkbox" id="isActive" name="isActive" data-toggle="toggle" data-on="Enabled" data-off="Disabled"data-size="mini" data-onstyle="success" data-offstyle="danger">
                        </div>
                        <input name="adminId" type="hidden" value="<?php echo $this->adminDetail->adminId; ?>"/>
                        <input name="contact_id" type="hidden" value="<?php echo $this->adminDetail->id; ?>"/>
                    </legend>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <div class="col-xs-4 control-label pull-left" for="porfile_image">
                            <a id="upload_image" data-toggle="modal" data-target="#myModal">
                                <div class="col-xs-12 for_porfile_image">
                                    <img id="porfile_image" class="img-responsive img-thumbnail" src="<?php echo $this->adminDetail->image; ?>"/>
                                </div>
                            </a>
                            <span class="help-block"></span>  
                        </div>  
                        <div class="col-xs-8">
                            <input name="firstname" type="text" placeholder="First Name" value="<?php echo $this->adminDetail->firstname ?>" class="form-control input-md name" required="" >
                        </div>
                        <div class="col-xs-8">
                            <input name="middlename" type="text" placeholder="Middle Name" value="<?php echo $this->adminDetail->middlename ?>" class="form-control input-md name" >
                        </div>
                        <div class="col-xs-8">
                            <input name="lastname"  type="text" placeholder="Last Name" value="<?php echo $this->adminDetail->lastname ?>" class="form-control input-md name" required="" >
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-xs-4 control-label" for="brithday">Date of Birth</label>  
                        <div class="col-xs-8">
                            <input id="brithday" name="dob" type="text" value="<?php echo $this->adminDetail->dob ?>" class="form-control input-md" required="">
                        </div>
                    </div>

                    <!-- Multiple Radios (inline) -->
                    <div class="form-group">
                        <label class="col-xs-4 control-label" for="radios">Gender</label>
                        <div class="col-xs-8"> 
                            <label class="radio-inline" for="radios-0">
                                <input type="radio" name="gender" id="radios-0" value="Male" checked="checked">
                                Male
                            </label> 
                            <label class="radio-inline" for="radios-1">
                                <input type="radio" name="gender" id="radios-1" value="Female">
                                Female
                            </label>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-xs-4 control-label" for="email">Email</label>  
                        <div class="col-xs-8">
                            <input id="name" name="email" type="text" value="<?php echo $this->adminDetail->email ?>" class="form-control input-md" required="">
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-xs-4 control-label" for="mobile">Phone</label>  
                        <div class="col-xs-8">
                            <input id="name" name="mobile" type="text" value="<?php echo $this->adminDetail->mobile ?>" class="form-control input-md" required="">
                        </div>
                    </div>


                </div>
                <div class="col-xs-6">
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-xs-4 control-label" for="name">Org Name</label>  
                        <div class="col-xs-8">
                            <input id="name" name="org_name" type="text" value="<?php echo $this->adminDetail->orgName ?>" class="form-control input-md" required="" >
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-xs-4 control-label" for="name">School Name</label>  
                        <div class="col-xs-8">
                            <input id="name" name="school_name" type="text" value="<?php echo $this->adminDetail->schoolName ?>" class="form-control input-md" required="" >
                        </div>
                    </div>


                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-xs-4 control-label" for="textinput">Full Address</label>  
                        <div class="col-xs-8">
                            <input id="autocomplete" name="full_address" type="text" value = "<?php echo $this->adminDetail->full_address ?>" placeholder="Enter your address" class="form-control input-md" data-validate="required">
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-xs-4 control-label" for="textinput">Street address</label>
                        <div class="col-xs-3">
                            <input id="street_number" name="street_number" type="text" value = "<?php echo $this->adminDetail->street_number ?>" placeholder="number" class="form-control input-md" required="" >
                        </div>
                        <div class="col-xs-5">
                            <input id="route" name="address" type="text" value = "<?php echo $this->adminDetail->address ?>" placeholder="street" class="form-control input-md" required="" >
                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-xs-4 control-label" for="suburb">Suburb</label>  
                        <div class="col-xs-8">
                            <input id="administrative_area_level_1" name="suburb" type="text" value = "<?php echo $this->adminDetail->suburb ?>" placeholder="suburb" class="form-control input-md" >

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-xs-4 control-label" for="suburb">City</label>  
                        <div class="col-xs-4">
                            <input id="locality" name="city" type="text" value = "<?php echo $this->adminDetail->city ?>" placeholder="city" class="form-control input-md" required="">
                        </div>
                        <div class="col-xs-4">
                            <input id="postal_code" name="postcode" type="text" value = "<?php echo $this->adminDetail->postcode ?>" placeholder="post code" class="form-control input-md">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="submitAdminDetail"></label>
                        <div class="col-xs-10">                     
                            <input name="submitAdminDetail" class="btn btn-primary" type="submit" value="Update Admin"/>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" id="imageForm" name="noteForm" action="<?php echo URL ?>admin/imageUpload" enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Upload Profile Image</h4>
                    </div>
                    <div class="modal-body">
                        <input id="file" type="file" name="file" />
                        <input name="userId" type="hidden" value="<?php echo $this->adminDetail->adminId; ?>"/>
                        <input name="role" type="hidden" value="<?php echo $this->adminDetail->title; ?>"/>
                        <input name="org_id" type="hidden" value="<?php echo $this->adminDetail->orgId; ?>"/>
                        <input name="contact_id" type="hidden" value="<?php echo $this->adminDetail->id; ?>"/>
                        <div class="progress">
                            <div class="bar"></div>
                        </div>
                        <div id="status"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" id="saveButton" name="saveButton" data-loading-text="Uploading..." class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var isActive = <?php echo $this->adminDetail->isActive; ?>;
            var gender = "<?php echo $this->adminDetail->gender; ?>";
            aa = $("input[name='gender'][value='"+gender+"']").attr('checked','checked');
            if (isActive)
            {
                $('#isActive').bootstrapToggle('on');
            } else {
                $('#isActive').bootstrapToggle('off');
            }

            $('#brithday').datepicker({
                format: "yyyy-mm-dd"
            });

            var bar = $('.bar');
            var percent = $('.percent');
            var status = $('#status');
            var $btn = $("#saveButton");
            var url = "<?php echo $this->adminDetail->image; ?>";

            $('#imageForm').ajaxForm({
                beforeSend: function () {
                    status.empty();
                    var percentVal = '0%';
                    bar.width(percentVal);
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    $btn.button('loading');
                    var percentVal = percentComplete + '%';
                    bar.width(percentVal);
                },
                complete: function (xhr) {
                    var txt = JSON.parse(xhr.responseText);
                    var control = $("#file");
                    if (txt.status == 1)
                    {
                        $('#my_image').attr('src', 'second.jpg');
                        status.html("upload successfully!");
                    } else {
                        status.html("upload fail!");
                    }
                    control.replaceWith(control = control.clone(true));
                    $btn.button('reset');
                    setTimeout(function () {
                        window.location.reload();
                    }, 500);
                }
            });
        });
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdAcc2hvfqZKLctapVS7DLiPNQCATwKjU&signed_in=true&libraries=places&callback=initAutocomplete"></script>

</div>