<div class="container">
    <?php
    $link = Session::get('permession');
    $school_id = Session::get('school');
    if ($school_id == null) {
        $url = URL . "admin/AdminData";
    } else {
        $url = URL . "admin/AdminData" . $school_id;
    }
    ?>
    <script>
        function Active(value, row, index) {
            if (value === "1")
            {
                return ['<span class="label label-success">Active</span>'].join('');
            } else {
                return ['<span class="label label-default">No Active</span>'].join('');
            }
        }
    </script>
    <table class="table" id="table" data-toggle="table" data-url="<?php echo $url; ?>" data-cache="false" data-height="299">
        <thead>
            <tr>
                <th data-field="id">#</th>
                <th data-field="firstname">First Name</th>
                <th data-field="lastname">Last Name</th>
                <th data-field="email">Email</th>
                <th data-field="name" >School</th>
                <th data-field="isActive" data-formatter="Active">Status</th>
                <th data-field="Edit" data-formatter="Edit" data-events="operateEvents"></th>
                <th data-field="Delete" data-formatter="Delete" data-events="operateEvents"></th>
            </tr>
        </thead>
    </table>

    <script>
        var $table = $('#table');
        function Edit(value, row, index) {
            var link = "<?php echo URL . $link['Learn Service']['Update Admin']; ?>" + row.id;
            return ['<a class="btn edit" href="' + link + '" title="Edit" >Edit</button>'].join('');
        }
        function Delete(value, row, index) {
            return ['<a class="btn remove" href="javascript:void(0)" title="Remove" >Delete</a>'].join('');
        }
        window.operateEvents = {
            'click .remove': function (e, value, row, index) {
                $.get("<?php echo URL . $link['Learn Service']['Delete School']; ?>" + row.id, function (data)
                {
                    alert(data);
                }
                )
            }
        };
    </script>
</div>