<div class="container">
    <div class="col-xs-8 col-xs-offset-2">
        <form class="form-horizontal" method="post" action="<?php echo URL ?>manager/addNew">
            <fieldset>

                <!-- Form Name -->
                <legend>New Manager</legend>

                <!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="org">Select Org</label>
                    <div class="col-md-6">
                        <select id="org_id" name="org_id" class="form-control" data-validate="required">
                            <option value="">--Select Org--</option>
                            <?php foreach ($this->orgs as $org): ?>
                                <option value="<?php echo $org->id ?>"><?php echo $org->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="username">username</label>  
                    <div class="col-md-6">
                        <input id="username" name="username" type="text" placeholder="username" class="form-control input-md" data-validate="required">

                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="password">password</label>
                    <div class="col-md-6">
                        <input id="password" name="password" type="password" placeholder="password" class="form-control input-md" data-validate="required">

                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="password2">repeat password</label>
                    <div class="col-md-6">
                        <input id="password2" name="password2" type="password" placeholder="password" class="form-control input-md" data-validate="required">
                    </div>
                </div>
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="email">Email</label>  
                    <div class="col-md-6">
                        <input id="email" name="email" type="text" placeholder="Email" class="form-control input-md" data-validate="emailAvailable" data-validate="required">
                    </div>
                </div>
                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-6">
                        <input class="btn btn-primary" type="submit" value="Next Step"/>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
    <script>
        $.verify.addRules({
            usernameAvailable: {
                fn: function (r) {
                    var n = r.val();
                    if (n != "")
                    {
                        var text = nameValidator(n);
                        if (text == 'yes')
                        {
                            return true;
                        } else {
                            return "Please change other username!";
                        }
                    } else {
                        return "Please type a username!";
                    }
                }
            },
            emailAvailable: {
                fn: function (r) {
                    var n = r.val();
                    if (n != "")
                    {
                        var text = emailValidator(n);
                        if (text == 'yes')
                        {
                            return true;
                        } else {
                            return "Please change other email!";
                        }
                    } else {
                        return "Please type a username!";
                    }
                }
            }
        });

        function nameValidator(n) {
            var content;
            $.ajax({
                url: "<?php echo URL ?>manager/usernameAvailable",
                type: "post",
                data: {"username": n},
                async: false,
                success:
                        function (msg) {
                            content = msg;
                        }
            });
            return content;
        }
        function emailValidator(n) {
            var content;
            $.ajax({
                url: "<?php echo URL ?>manager/emailAvailable",
                type: "post",
                data: {"email": n},
                async: false,
                success:
                        function (msg) {
                            content = msg;
                        }
            });
            return content;
        }

    </script>
</div>
