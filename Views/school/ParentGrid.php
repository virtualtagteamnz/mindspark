<div id="parentToolbar">
    <a href="<?php echo URL . $link['Learn Service']['Create Parent']; ?>" type="button" class="btn btn-default">New Parent</a>
</div>
<table class="table" id="table" data-toggle="table" data-url="<?php echo URL . "school/parentData/" . $this->schoolId; ?>" 
       data-cache="false"
       data-height="500" 
       data-click-to-select="true"
       data-toolbar="#parentToolbar"
       data-show-columns="true" 
       >
    <thead>
        <tr>
            <th data-field="id">#</th>
            <th data-field="full_name">First Name</th>
            <th data-field="email" data-visible="false">Email</th>   
            <th data-field="isActive"  data-formatter="Active">Stats</th>     
            <th data-field="" data-formatter="ViewParent"></th>
        </tr>
    </thead>
</table>
<script>
    function ViewParent(value, row, index) {
        var link = "<?php echo URL . $link['Learn Service']['View Parents'] . "view/"; ?>" + row.id;
        return ['<a class="btn" href="' + link + '" title="view">View</a>'].join('');
    }
    function Active(value, row, index) {
        if (value === "1")
        {
            return ['<span class="label label-success">Active</span>'].join('');
        } else {
            return ['<span class="label label-default">No Active</span>'].join('');
        }
    }
</script>
