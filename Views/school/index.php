<div class="container">
    <?php $link = Session::get('permession');
    ?>
    <script>
        var $table = $('#table');
        function Active(value, row, index) {
            if (value === "1")
            {
                return ['<span class="label label-success">Active</span>'].join('');
            } else {
                return ['<span class="label label-default">No Active</span>'].join('');
            }
        }
        function Edit(value, row, index) {
            var link = "<?php echo URL . $link['Learn Service']['Update School']; ?>" + row.id;
            return ['<a class="btn edit" href="' + link + '" title="Edit" >Edit</button>'].join('');
        }
        function Delete(value, row, index) {
            return ['<a class="btn remove" href="javascript:void(0)" title="Remove" >Delete</a>'].join('');
        }
        window.operateEvents = {
            'click .remove': function (e, value, row, index) {
                $.get("<?php echo URL . $link['Learn Service']['Delete School']; ?>" + row.id, function (data)
                {
                    alert(data);
                }
                )

            }
        };
    </script>

    <table class="table" id="table" data-toggle="table" data-url="<?php echo URL; ?>/school/schoolData" data-cache="false" data-height="299">
        <thead>
            <tr>
                <th data-field="name">School Name</th>
                <th data-field="isActive" data-formatter="Active">Status</th>
                <th data-field="Edit" data-formatter="Edit" data-events="operateEvents"></th>
                <th data-field="Delete" data-formatter="Delete" data-events="operateEvents"></th>
            </tr>
        </thead>
    </table>
</div>