<div class="container">
    <div class="col-xs-8 col-xs-offset-2 border">
        <form class="form-horizontal" method="post" name="schoolForm" id="schoolForm" action="<?php echo URL ?>school/newSchool">
            <fieldset>
                <!-- Form Name -->
                <legend>New School</legend>

                <!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="school">Select Org</label>
                    <div class="col-md-6">
                        <select id="school" name="org_id" class="form-control" data-validate="schoolNameAvailable,required">
                            <option value="" >--Select School--</option>
                            <?php foreach ($this->orgs as $org): ?>
                                <option value="<?php echo $org->id ?>"><?php echo $org->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>


                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="name">Name</label>  
                    <div class="col-md-6">
                        <input id="name" name="name" type="text" placeholder="school" class="form-control input-md" data-validate="required">
                    </div>
                </div>

                <!-- Multiple Radios (inline) -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="role">Role</label>
                    <div class="col-md-4"> 
                        <label class="radio-inline" for="role-0">
                            <input type="radio" name="role" id="selectAll" value="true" checked="checked">
                            All roles
                        </label> 
                        <label class="radio-inline" for="role-1">
                            <input type="radio" name="role" id="selectRole" value="false">
                            select roles
                        </label>
                    </div>
                </div>

                <!-- Multiple Checkboxes -->
                <div class="form-group" id="roleSelect" style="display: none;">
                    <label class="col-md-4 control-label" for="role">Select role:</label>
                    <div class="col-md-6">
                        <div class="checkbox">
                            <label for="role-1">
                                <input type="checkbox" name="selectRole[]" id="role-1" value="2" checked="">
                                Admin
                            </label>
                        </div>
                        <div class="checkbox">
                            <label for="role-2">
                                <input type="checkbox" name="selectRole[]" id="role-2" value="3" checked="">
                                Manager
                            </label>
                        </div>
                        <div class="checkbox">
                            <label for="role-3">
                                <input type="checkbox" name="selectRole[]" id="role-3" value="4" checked=""> 
                                Teacher
                            </label>
                        </div>
                        <div class="checkbox">
                            <label for="role-4">
                                <input type="checkbox" name="selectRole[]" id="role-4" value="5" checked="">
                                Parent
                            </label>
                        </div>
                    </div>
                </div>

                <!-- Submit button (inline) -->
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-4"> 
                        <input class="btn btn-primary" name="submitSchoolForm" type="submit" value="Next Step"/>
                    </div>
                </div>
            </fieldset>
        </form>
        <script>
            $(document).ready(function () {
                $('input[name=role]:radio').change(function () {
                    var isAll = $('input[name="role"]:checked', '#schoolForm').val() === "true";
                    if (isAll)
                    {
                        $('#roleSelect').css({"display": "none"});
                    } else {
                        $('#roleSelect').css({"display": "block"});
                    }
                })
            }
            );
            $.verify.addRules({
                schoolNameAvailable: {
                    fn: function (r)
                    {
                        var n = r.val();
                        console.log(n);
                        var text = schoolNameValidator(n);
                        if (!text)
                        {
                            return "Please type other school name!";
                        } else {
                            return true;
                        }
                    }
                }
            });
            function schoolNameValidator(n) {
                var isSuccess = false;
                $.ajax({
                    url: "<?php echo URL ?>school/schoolNameAvailable",
                    type: "post",
                    data: {"name": n},
                    async: false,
                    success:
                            function (msg) {
                                isSuccess = msg == "available" ? true : false
                            }
                });
                return isSuccess;
            }
        </script>
    </div>
</div>