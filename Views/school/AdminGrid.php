<div id="adminToolbar">
    <a href="<?php echo URL . $link['Learn Service']['Create Admin']; ?>" type="button" class="btn btn-default">New Admin</a>
</div>
<table class="table" id="table" data-toggle="table" data-url="<?php echo URL . "school/adminData/" . $this->schoolId; ?>" 
       data-cache="false"
       data-height="500" 
       data-click-to-select="true"
       data-toolbar="#adminToolbar"
       data-show-columns="true" 
       >
    <thead>
        <tr>
            <th data-field="id">#</th>
            <th data-field="full_name">Full Name</th>
            <th data-field="email" data-visible="false">Email</th>   
            <th data-field="isActive"  data-formatter="Active">Stats</th>     
            <th data-field="" data-formatter="ViewAdmin"></th>
        </tr>
    </thead>
</table>
<script>
    function ViewAdmin(value, row, index) {

        var link = "<?php echo URL . $link['Learn Service']['View Admin'] . "view/"; ?>" + row.id;
        return ['<a class="btn" href="' + link+ '" title="view">View</a>'].join('');
    }
    function Active(value, row, index) {
        if (value === "1")
        {
            return ['<span class="label label-success">Active</span>'].join('');
        } else {
            return ['<span class="label label-default">No Active</span>'].join('');
        }
    }
</script>