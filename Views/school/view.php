<div class="container">
    <?php $link = Session::get('permession'); ?>
    <div class="col-xs-12 col-sm-4">
        <h3>Admin list</h3>
        <hr/>
        <?php require_once "AdminGrid.php"; ?>
    </div>
    <div class="col-xs-12 col-sm-4">
        <h3>Teacher list</h3>
        <hr/>
        <?php require_once 'TeacherGrid.php'; ?>
    </div>
    <div class="col-xs-12 col-sm-4">
        <h3>Parent list</h3>
        <hr/>
        <?php require_once 'ParentGrid.php'; ?>
    </div>
</div>
