<div class="container">
    <?php $link = Session::get('permession'); ?>
    <div id="grid"></div>
    <script>
        var orgListDataScource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: "<?php echo URL ?>LearnService/getOrg",
                    dataType: "json"
                }
            },
            schema: {
                model: {
                    id: "oId",
                    fields: {
                        oId: {editable: false},
                        oName: {},
                        contactId: {editable: false},
                        city: {},
                        full_address: {},
                        introduction: {}
                    }
                }
            },
            pageSize: 20
        });
        var grid = $("#grid").kendoGrid({
            dataSource: orgListDataScource,
            pageable: true,
            height: 430,
            columns: [
                {field: "oId", title: "ID", width: "80px"},
                {field: "oName", title: "Name"},
                {field: "contactId", title: "state", hidden: true},
                {field: "city", title: "City"},
                {field: "full_address", title: "Full Address"},
                {field: "introduction", title: "Introduction"},
                {
                    template: '<a href="<?php echo URL; ?>org/view/#=oId#">View</a>'
                }
            ]
        }).data("kendoGrid");
    </script>

</div>