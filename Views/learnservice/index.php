<div class="container">
    <?php $link = Session::get('permession'); ?>
    <div class="col-xs-12">
        <div id="toolbar">
            <a href="<?php echo URL.$link['Learn Service']['Create Org']; ?>" type="button" id="addOrg" class="btn btn-default">Add New Org</a>
        </div>
        <table class="table" id="table" data-toggle="table" data-url="<?php echo URL ?>LearnService/getOrg" 
               data-cache="false"
               data-height="500" 
               data-click-to-select="true"
               data-toolbar="#toolbar"
               data-show-refresh="true" 
               data-show-columns="true" 
               data-search="true"
               >
            <thead>
                <tr>
                    <th data-field="oId">#</th>
                    <th data-field="oName">Name</th>
                    <th data-field="address" data-visible="false">Street</th>
                    <th data-field="suburb" data-visible="false">Suburb</th>
                    <th data-field="city" data-visible="false">City</th>
                    <th data-field="full_address">Full address</th>
                    <th data-field="introduction">Introduction</th>       
                    <th data-field="" data-formatter="ViewSchool"></th>
                </tr>
            </thead>
        </table>
    </div>
    <script>
        function ViewSchool(value, row, index) {
            var link = "<?php echo URL . $link['Learn Service']['View Org'] . "view/"; ?>" + row.oId;
            return ['<a class="btn" href="' + link + '" title="view">View</a>'].join('');
        }
    </script>

</div>