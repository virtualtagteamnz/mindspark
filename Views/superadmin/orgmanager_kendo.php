<div class="container">
    <form class="form-horizontal" method="post" action="<?php echo URL ?>superadmin/orgupdate">
        <div class="col-xs-6 col-xs-offset-3">
            <div id="grid"></div>
        </div>
    </form>
    <script>
//        $(document).ready(function () {

        var dataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: "<?php echo URL ?>superadmin/orgData",
                    dataType: "json"
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: {editable: false},
                        name: {},
                        state: {type: "boolean", }
                    }
                }
            },
            pageSize: 20
        });

        var grid = $("#grid").kendoGrid({
            dataSource: dataSource,
            pageable: true,
            height: 430,
            dataBound: onDataBound,
            columns: [
                {template: '<input type="checkbox" class="checkbox" #= state ? "checked=checked" : ""# />', width: "35px"},
                {field: "id", title: "ID", width: "80px"},
                {field: "name", title: "Name"},
                {field: "state", title: "state"}
            ],
            toolbar: [
                {
//                "save"
                    template: '<a class="k-button" href="\\#" onclick="updateOrg()">Update Org</a>'
                }
            ]

        }).data("kendoGrid");
        grid.table.on("click", ".checkbox", selectRow);

        var checkedIds = {};
        function selectRow() {
            var checked = this.checked,
                    row = $(this).closest("tr"),
                    grid = $("#grid").data("kendoGrid"),
                    dataItem = grid.dataItem(row);
            console.log(row + " = " + checked);
            checkedIds[dataItem.id] = checked;

            if (checked) {
                //-select the row
                dataItem.set("state", checked);
                row.addClass("k-state-selected");
            } else {
                //-remove selection
                dataItem.set("state", checked);
                row.removeClass("k-state-selected");
            }
        }

        function onDataBound(e) {
            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {
                if (checkedIds[view[i].id]) {
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .addClass("k-state-selected")
                            .find(".checkbox")
                            .attr("checked", "checked");
                }
            }
        }
        function updateOrg() {
            var checked = [];
            for (var i in checkedIds) {
                if (checkedIds[i]) {
                    checked.push(i);
                }
            }
            var view = grid.dataSource.view();
//            view = JSON.stringify(view);
            sendUpdateRequest(view);
            return false;
        }
        function sendUpdateRequest(view)
        {
            console.log(view)
            console.log(JSON.stringify(view));
            $.ajax({
                url: "<?php echo URL ?>superadmin/orgupdate",
                type: 'POST',
                data: {
                    models: JSON.stringify(view)
                },
//                data: [{"name":"abc"}],
                complete: function (jqXHR, textStatus) {
                    console.log(jqXHR.responseText);
                    if (jqXHR.responseText == "yes")
                    {
                        window.location.reload();
                    }
                }
            })
        }
//        });

    </script>
</div>
