<div class="container">
    <form id="orgManagmentForm" class="form-horizontal" method="post" action="<?php echo URL ?>superadmin/orgupdate">
        <div id="toolbar">
            <a type="button" id="updateOrg" class="btn btn-default">Update</a>
        </div>
        <div class="col-xs-6 col-xs-offset-3">
            <table class="table" id="table" data-toggle="table" data-url="<?php echo URL ?>superadmin/orgData" 
                   data-cache="false"
                   data-height="299" 
                   data-click-to-select="true"
                   data-toolbar="#toolbar"
                   data-show-refresh="true" 
                   data-show-toggle="true" 
                   data-show-columns="true" 
                   data-search="true"
                   >
                <thead>
                    <tr>
                        <th class="bs-checkbox " style="width: 36px;" data-field="state" tabindex="0" data-checkbox="true">
                            <input name="btSelectAll" type="checkbox"/>
                        </th>
                        <th data-field="id">id</th>
                        <th data-field="name">Name</th>
                        <th data-field="introduction">Introduction</th>
                    </tr>
                </thead>
            </table>
        </div>
    </form>
    <script>
        var $table = $('#table');
        $('#updateOrg').click(function () {
            var post = $("#orgManagmentForm").attr("action");
            $.ajax({
                url: post,
                method: "post",
                data: {
                    models: JSON.stringify($table.bootstrapTable('getData'))
                },
                success: function (data, textStatus, jqXHR) {
                    console.log(data);
                    $table.bootstrapTable('refresh', {
                        silent: false,
                        url: "<?php echo URL ?>superadmin/orgData"
                    });
                }
            });
        });
    </script>
</div>