<?php

class assessment extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::checkLogin();
    }

    function index() {
        $this->view->title = "Dashboard | Assessment";
        $this->view->msg = "welcome Assessment";
        $this->view->render("assessment/index");
    }

    function create() {
        $this->view->title = "Dashboard | Assessment";
        $this->view->msg = "create Assessment";
        $this->view->render("assessment/index");
    }

    function update() {
        $this->view->title = "Dashboard | Assessment";
        $this->view->msg = "update Assessment";
        $this->view->render("assessment/index");
    }

    function delete() {
        $this->view->title = "Dashboard | Assessment";
        $this->view->msg = "delete Assessment";
        $this->view->render("assessment/index");
    }

}
