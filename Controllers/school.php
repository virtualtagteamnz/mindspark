<?php

class school extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::checkLogin();
    }

    function index() {
        $this->view->title = "Dashboard | School";
        $this->view->msg = "welcome School";
        $this->view->publicCSS = array('bootstrap-table.min.css');
        $this->view->publicJS = array('bootstrap-table.min.js');
        $this->view->render("school/index");
    }

    function view($schooId = null) {
        if ($schooId != null) {
            $this->view->title = "Dashboard | Learn Service";
            $this->view->publicCSS = array('bootstrap-table.min.css');
            $this->view->publicJS = array('bootstrap-table.min.js');
            $this->view->schoolId = $schooId;
            $this->view->render("school/view");
        } else {
            Session::set("error", "school not found");
            header("Location:" . URL . "error/fail/SchoolError");
        }
    }

    function schoolData() {
        echo json_encode($this->model->getAllSchool());
    }

    function adminData($schoolId) {
        echo json_encode($this->model->getAllAdmin($schoolId));
    }
    function teacherData($schoolId) {
        echo json_encode($this->model->getAllTeacher($schoolId));
    }
    function parentData($schoolId) {
        echo json_encode($this->model->getAllParent($schoolId));
    }

    function create() {
        $this->view->title = "Dashboard | School >> New School";
        $this->view->msg = "create School";
        $this->view->orgs = $this->model->selectOrgs();
        $this->view->render("school/createForm");
    }

    function newSchool() {
        $this->view->title = "Dashboard | School >> New School";
        if (isset($_POST)) {
            try {
                $form = new Form();
                $isAll = $_POST['role'];
                if ($isAll === 'true') {
                    $form->post('org_id')
                            ->post('name')
                            ->post('role');
                } else {
                    $form->post('org_id')
                            ->post('name')
                            ->post('role')
                            ->post('selectRole');
                }
                $form->submit();
                $data = $form->fetch();
                $newSchool = $this->model->addNewSchool($data);
                if ($newSchool) {
                    header("Location:" . URL . "school/schoolDetail/" . $newSchool->id);
                } else {
                    $this->view->error = 'Add school unsuccessfully!';
                    $this->view->render("error/fail");
                }
            } catch (Exception $exc) {
                $this->view->error = $exc;
                $this->view->render("error/fail");
            }
        }
    }

    function schoolDetail($schoolId = NULL) {
        if ($schoolId == null || empty($schoolId)) {
            $this->view->error = 'Please select a school';
            header("Location:" . URL . "error/fail");
        }
        if ($this->model->checkSchoolId($schoolId)) {
            $this->view->title = "Dashboard | Superadmin";
            $this->view->publicCSS = array("summernote.css", "bootstrap-toggle.min.css");
            $this->view->publicJS = array("summernote.min.js", "bootstrap-toggle.min.js");
            $this->view->js = array("org/js/autoAddress.js");
            $this->view->schoolDetail = $this->model->selectSchoolContactDetail($schoolId);
            $this->view->render("school/schoolDetailForm");
        } else {
            Session::set("error", "please select a school");
            header("Location:" . URL . "error/fail/SchoolError");
        }
    }

    function schoolDetailForm($schoolId = NULL) {
        if ($schoolId != NULL) {
            if (isset($_POST)) {
                $form = new Form();
                $form->post('id')
                        ->post('site_contact_id')
                        ->post('name')
                        ->post('landline')
                        ->post('mobile')
                        ->post('fax')
                        ->post('full_address')
                        ->post('street_number')
                        ->post('address')
                        ->post('suburb')
                        ->post('city')
                        ->post('postcode')
                        ->val('digit')
                        ->post('introduction')
                        ->post('description')
                        ->post('isActive');
                $form->submit();
                $data = $form->fetch();
                $isAdd = $this->model->updateSchoolDetail($data);
                if ($isAdd) {
                    header("Location:" . URL . "school");
                } else {
                    Session::set("error", "add school detail failed");
                    header("Location:" . URL . "error/fail/schoolError");
                }
            } else {
                Session::set("error", "no school data");
                header("Location:" . URL . "error/fail/schoolError");
            }
        } else {
            Session::set("error", "no school select");
            header("Location:" . URL . "error/fail/schoolError");
        }
    }

    function schoolNameAvailable() {
        if (isset($_POST)) {
            $data = $_POST['name'];
            $isAvailable = $this->model->schoolNameAvailable($data);
            if ($isAvailable) {
                echo "exist";
            } else {
                echo "available";
            }
        }
    }

    function update($id) {
        $this->view->title = "Dashboard | School";
        $this->view->publicCSS = array("summernote.css", "bootstrap-toggle.min.css");
        $this->view->publicJS = array("summernote.min.js", "bootstrap-toggle.min.js");
        $this->view->css = array("org/css/OrgDetailForm.css");
        $this->view->js = array("org/js/autoAddress.js");
        $this->view->schoolDetail = $this->model->selectSchoolContactDetail($id);
        $this->view->render("school/update");
    }

    function delete($id) {
        echo 'delete ' . $id;
    }

}
