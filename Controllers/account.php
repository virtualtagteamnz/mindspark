<?php

class account extends Controller {

    public function __construct() {
        parent::__construct();
        Session::init();
    }

    function login() {
        $this->view->title = 'Login';
        if (isset($_POST)) {
            try {
                $form = new Form();
                $form->post('username')
                        ->post('password');
                $form->submit();
                $data = $form->fetch();
                $user = null;
                $user = $this->model->login($data);
                if (!empty($user) && $user != null) {
                    $permession = $this->model->userPremession($user->id);
                    Session::set("uid", $user->id);
                    Session::set("firstname", $user->firstname);
                    Session::set("lastname", $user->lastname);
                    Session::set("role", $user->role_name);
                    Session::set("school", $user->school_id);
                    Session::set("permession", $permession);
                    Session::set("loggedIn", TRUE);
                    $org = $this->model->findOrg($user->school_id, $user->role_name);
                    Session::set("org", $org);

                    header("Location:" . URL . "dashboard");
                } else {
                    Session::destroy();
                    header("Location:" . URL . "index?login=f");
                }
            } catch (Exception $ex) {
                Session::destroy();
                header("Location:" . URL . "index?login=f");
            }
        }
    }

    function logout() {
        $islogin = Auth::checkLogin();
        if ($islogin) {
            Session::destroy();
            header("Location:" . URL);
        }
    }

}
