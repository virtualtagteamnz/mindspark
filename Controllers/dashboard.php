<?php

class dashboard extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::checkLogin();
    }
    function index() {
        $this->view->title = "Dashboard";
        $this->view->render("dashboard/index");
    }

}
