<?php

class org extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::checkLogin();
    }

    function index() {
        $this->view->title = "Dashboard | Org";
        $this->view->publicCSS = array('bootstrap-table.min.css');
        $this->view->publicJS = array('bootstrap-table.min.js');
        $this->view->render("org/index");
    }

    function view($orgId = null) {
        if ($orgId != null) {
            $this->view->publicCSS = array('bootstrap-table.min.css');
            $this->view->publicJS = array('bootstrap-table.min.js');
            $this->view->orgId = $orgId;
            $this->view->render("org/view");
        } else {
            Session::set("error", "org not found");
            header("Location:" . URL . "error/fail/OrgError");
        }
    }

    function orgData() {
            echo json_encode($this->model->getAllSchool());
    }

    function schoolData($orgId) {
        if ($orgId != null) {
            echo json_encode($this->model->getAllSchool($orgId));
        } else {
            Session::set("error", "org not found");
            header("Location:" . URL . "error/fail/OrgError");
        }
    }
     function managerData($orgId) {
        if ($orgId != null) {
            echo json_encode($this->model->getAllManager($orgId));
        } else {
            Session::set("error", "org not found");
            header("Location:" . URL . "error/fail/OrgError");
        }
    }

    function create() {
        $this->view->title = "Dashboard | Superadmin";
        $this->view->render("org/createOrgForm");
    }

    function newOrg() {
        $this->view->title = "Dashboard | School >> New Org";
        if (isset($_POST)) {
            try {
                $form = new Form();
                $form->post('name');
                $form->submit();
                $data = $form->fetch();
                $newOrg = $this->model->createNewOrg($data);
                if (!empty($newOrg)) {
                    header("Location:" . URL . "org/orgDetail/" . $newOrg->id);
                } else {
                    Session::set("error", "new org fail");
                    header("Location:" . URL . "error/fail/OrgError");
                }
            } catch (Exception $exc) {
                Session::set("error", $exc);
                header("Location:" . URL . "error/fail/OrgError");
            }
        }
    }

    function orgDetail($orgId = NULL) {
        if ($orgId == null || empty($orgId)) {
            $this->view->error = 'Please select a org';
            header("Location:" . URL . "error/fail");
        }
        if ($this->model->checkOrgId($orgId)) {
            $this->view->title = "Dashboard | Superadmin";
            $this->view->publicCSS = array("summernote.css", "bootstrap-toggle.min.css");
            $this->view->publicJS = array("summernote.min.js", "bootstrap-toggle.min.js");
            $this->view->js = array("org/js/autoAddress.js");
            $this->view->orgDetail = $this->model->selectOrgContactDetail($orgId);
            $this->view->render("org/orgDetailForm");
        } else {
            Session::set("error", "please select a org");
            header("Location:" . URL . "error/fail/OrgError");
        }
    }

    function orgAvailable() {
        if (isset($_POST)) {
            $orgName = $_POST['name'];
            $s = $this->model->checkOrgName($orgName);
            if ($s) {
                echo "exist";
            } else {
                echo "available";
            }
        }
    }

    function orgDetailForm($orgId = NULL) {
        if ($orgId != NULL) {
            if (isset($_POST)) {
                $form = new Form();
                $form->post('id')
                        ->post('site_contact_id')
                        ->post('name')
                        ->post('landline')
                        ->post('mobile')
                        ->post('fax')
                        ->post('full_address')
                        ->post('street_number')
                        ->post('address')
                        ->post('suburb')
                        ->post('city')
                        ->post('postcode')
                        ->val('digit')
                        ->post('introduction')
                        ->post('description')
                        ->post('isActive');
                $form->submit();
                $data = $form->fetch();
                $isAdd = $this->model->updateOrgDetail($data);
                if ($isAdd) {
                    header("Location:" . URL . "org");
                } else {
                    Session::set("error", "add org detail failed");
                    header("Location:" . URL . "error/fail/OrgError");
                }
            } else {
                Session::set("error", "no org data");
                header("Location:" . URL . "error/fail/OrgError");
            }
        } else {
            Session::set("error", "no org select");
            header("Location:" . URL . "error/fail/OrgError");
        }
    }

//    function orgInfo($orgId) {
//        $this->view->title = "Dashboard | Org";
//        $this->view->org = $this->model->view($orgId);
//        $this->view->render("org/update");
//    }
    function update($orgId) {
        $this->view->title = "Dashboard | Superadmin";
        $this->view->publicCSS = array("summernote.css", "bootstrap-toggle.min.css");
        $this->view->publicJS = array("summernote.min.js", "bootstrap-toggle.min.js");
        $this->view->css = array("org/css/OrgDetailForm.css");
        $this->view->js = array("org/js/autoAddress.js");
        $this->view->orgDetail = $this->model->selectOrgContactDetail($orgId);
        $this->view->render("org/update");
    }

    function delete($id) {
        echo 'delete ' . $id;
    }

}
