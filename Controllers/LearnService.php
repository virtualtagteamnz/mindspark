<?php
class LearnService extends Controller{
    public function __construct() {
        parent::__construct();
        Auth::checkLogin();
    }
    function index() {
        $this->view->title = "Dashboard | Learn Service";
        $this->view->publicCSS = array('bootstrap-table.min.css');
        $this->view->publicJS = array('bootstrap-table.min.js');
        $this->view->render("learnservice/index");
    }
    function getOrg() {
         echo json_encode($this->model->getOrgList());
    }
}
