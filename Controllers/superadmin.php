<?php

class superadmin extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::checkLogin();
    }

    function index() {
        $this->view->title = "Dashboard | Superadmin";
        $this->view->msg = "welcome Superadmin";
        $this->view->render("superadmin/index");
    }

    function orgcontrol() {
        $this->view->title = "Dashboard | Org Manager";
        $this->view->publicCSS = array('bootstrap-table.min.css');
        $this->view->publicJS = array('bootstrap-table.min.js');
        $this->view->render("superadmin/orgmanager");
    }

    function orgData() {
        echo json_encode($this->model->getOrgList());
    }

    function orgupdate() {
        if ($_POST['models']) {
            $data = json_decode(stripslashes($_POST['models']));
            $oldData = $this->model->getOrgList();
            $toAdd = array();
            $toDelete = array();
            for ($index = 0; $index < count($oldData); $index++) {
                for ($i = 0; $i < count($data); $i++) {
                    if ($oldData[$index]->id == $data[$i]->id && $oldData[$index]->name == $data[$i]->name) {
                        if ($oldData[$index]->state == true && $data[$i]->state == false) {//delete
                            $toDelete[] = $data[$i];
                        } else if ($oldData[$index]->state == false && $data[$i]->state == true) {//add new 
                            $toAdd[] = $data[$i];
                        }
                    }
                }
            }

            $update = $this->model->updateOrgUser($toDelete, $toAdd);
            if ($update) {
                echo "yes";
            } else {
                echo "no";
            }
        }
    }

    function create() {
        $this->view->title = "Dashboard | Superadmin";
        $this->view->msg = "create Superadmin";
        $this->view->render("superadmin/index");
    }

    function update() {
        $this->view->title = "Dashboard | Superadmin";
        $this->view->msg = "update Superadmin";
        $this->view->render("superadmin/index");
    }

    function delete() {
        $this->view->title = "Dashboard | Superadmin";
        $this->view->msg = "delete Superadmin";
        $this->view->render("superadmin/index");
    }

}
