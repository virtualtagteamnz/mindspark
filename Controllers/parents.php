<?php

class parents extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::checkLogin();
    }

    function index() {
        $this->view->title = "Dashboard | Parents";
        $this->view->msg = "welcome Parents";
        $this->view->render("parents/index");
    }
    function create() {
        $this->view->title = "Dashboard | Parents";
        $this->view->orgs = $this->model->selectOrg();
        $this->view->render("parents/NewParentForm");
    }
    function addNew() {
        $this->view->title = "Dashboard | Parent >> New Parent";
        if (isset($_POST)) {
            try {
                $form = new Form();
                $form->post('org_id')
                        ->post('school_id_value')
                        ->post('username')
                        ->post('password')
                        ->post('email');
                $form->submit();
                $data = $form->fetch();
                $parent = $this->model->addNewParent($data);
                if ($parent) {
                    header("Location:" . URL . "parents/parentDetail/" . $parent);
                } else {
                    Session::set("error", "Add New Parent Error");
                    header("Location:" . URL . "error/fail/ParentError");
                }
            } catch (Exception $exc) {
                Session::set("error", "Add New Parent Error");
                header("Location:" . URL . "error/fail/ParentError");
            }
        }
    }
    
     function parentDetail($parent = null) {
        if ($parent == null || empty($parent)) {
            $this->view->error = 'Please select a parent';
            header("Location:" . URL . "error/fail");
        }
        if ($this->model->checkParentId($parent)) {
            $this->view->title = "Dashboard | Parent";
            $this->view->publicCSS = array("summernote.css", "bootstrap-toggle.min.css", "bootstrap-datepicker.min.css");
            $this->view->publicJS = array("summernote.min.js", "bootstrap-toggle.min.js", "bootstrap-datepicker.min.js", "jquery.form.js", "dropzone.js");
            $this->view->css = array("admin/css/adminStyle.css");
            $this->view->js = array("org/js/autoAddress.js");
            $this->view->parentDetail = $this->model->selectParentContactDetail($parent);
            $this->view->render("parents/parentDetail");
        } else {
            Session::set("error", "please select a parent");
            header("Location:" . URL . "error/fail/ParentError");
        }
    }
    function parentDetailForm($parentId = null) {
        if ($parentId != null) {
            if (isset($_POST)) {
                $form = new Form();
                $form->post('userId')
                        ->post('contact_id')
                        ->post('firstname')
                        ->post('middlename')
                        ->post('lastname')
                        ->post('dob')
                        ->post('gender')
                        ->post('email')
                        ->post('mobile')
                        ->post('org_name')
                        ->post('school_name')
                        ->post('full_address')
                        ->post('street_number')
                        ->post('address')
                        ->post('suburb')
                        ->post('city')
                        ->post('postcode')
                        ->post('isActive');
                $form->submit();
                $data = $form->fetch();
                $isAdd = $this->model->updateParentDetail($data);
                if ($isAdd) {
                    header("Location:" . URL . "parents");
                } else {
                    Session::set("error", "add parent detail failed");
                    header("Location:" . URL . "error/fail/parentError");
                }
            } else {
                Session::set("error", "no parent data");
                header("Location:" . URL . "error/fail/parentError");
            }
        } else {
            Session::set("error", "no parent select");
            header("Location:" . URL . "error/fail/parentError");
        }
    }
    function update() {
        $this->view->title = "Dashboard | Parents";
        $this->view->msg = "update Parents";
        $this->view->render("parents/index");
    }
    function delete() {
        $this->view->title = "Dashboard | Parents";
        $this->view->msg = "delete Parents";
        $this->view->render("parents/index");
    }
    

}
