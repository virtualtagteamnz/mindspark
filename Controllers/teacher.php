<?php

class teacher extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::checkLogin();
    }

    function index() {
        $this->view->title = "Dashboard | Teacher";
        $this->view->msg = "welcome teacher";
        $this->view->render("teacher/index");
    }

    function view($teacherId=null) {
          if ($teacherId == null) {
            $this->view->error = 'Please select a teacher';
            header("Location:" . URL . "error/fail");
        }
        $this->view->title = "Dashboard | Teacher";
        $this->view->publicCSS = array('bootstrap-table.min.css');
        $this->view->publicJS = array('bootstrap-table.min.js');
//        $this->view->teacher = $this->model->getSchoolByTeacher($teacherId);
        $this->view->render("teacher/view");
    }
    function create() {
        $this->view->title = "Dashboard | Teacher";
        $this->view->orgs = $this->model->selectOrg();
        $this->view->render("teacher/NewTeacherForm");
    }

    function addNew() {
        $this->view->title = "Dashboard | Teacher >> New Teacher";
        if (isset($_POST)) {
            try {
                $form = new Form();
                $form->post('org_id')
                        ->post('school_id_value')
                        ->post('username')
                        ->post('password')
                        ->post('email');
                $form->submit();
                $data = $form->fetch();
                $teacher = $this->model->addNewTeacher($data);
                if ($teacher) {
                    header("Location:" . URL . "teacher/teacherDetail/" . $teacher);
                } else {
                    Session::set("error", "Add New Teacher Error");
                    header("Location:" . URL . "error/fail/TeacherError");
                }
            } catch (Exception $exc) {
                Session::set("error", "Add New Teacher Error");
                header("Location:" . URL . "error/fail/TeacherError");
            }
        }
    }

    function teacherDetail($teacher = null) {
        if ($teacher == null || empty($teacher)) {
            $this->view->error = 'Please select a teacher';
            header("Location:" . URL . "error/fail");
        }
        if ($this->model->checkTeacherId($teacher)) {
            $this->view->title = "Dashboard | Teacher";
            $this->view->publicCSS = array("summernote.css", "bootstrap-toggle.min.css", "bootstrap-datepicker.min.css");
            $this->view->publicJS = array("summernote.min.js", "bootstrap-toggle.min.js", "bootstrap-datepicker.min.js", "jquery.form.js", "dropzone.js");
            $this->view->css = array("admin/css/adminStyle.css");
            $this->view->js = array("org/js/autoAddress.js");
            $this->view->teacherDetail = $this->model->selectTeacherContactDetail($teacher);
            $this->view->render("teacher/teacherDetail");
        } else {
            Session::set("error", "please select a teacher");
            header("Location:" . URL . "error/fail/TeacherError");
        }
    }
    function teacherDetailForm($teacherId = null) {
        if ($teacherId != null) {
            if (isset($_POST)) {
                $form = new Form();
                $form->post('userId')
                        ->post('contact_id')
                        ->post('firstname')
                        ->post('middlename')
                        ->post('lastname')
                        ->post('dob')
                        ->post('gender')
                        ->post('email')
                        ->post('mobile')
                        ->post('org_name')
                        ->post('school_name')
                        ->post('full_address')
                        ->post('street_number')
                        ->post('address')
                        ->post('suburb')
                        ->post('city')
                        ->post('postcode')
                        ->post('isActive');
                $form->submit();
                $data = $form->fetch();
                $isAdd = $this->model->updateTeacherDetail($data);
                if ($isAdd) {
                    header("Location:" . URL . "teacher");
                } else {
                    Session::set("error", "add teacher detail failed");
                    header("Location:" . URL . "error/fail/teacherError");
                }
            } else {
                Session::set("error", "no teacher data");
                header("Location:" . URL . "error/fail/teacherError");
            }
        } else {
            Session::set("error", "no teacher select");
            header("Location:" . URL . "error/fail/teacherError");
        }
    }
    function update($teacherId) {
        if ($teacherId == null || empty($teacherId)) {
            $this->view->error = 'Please select a teacher';
            header("Location:" . URL . "error/fail");
        }
         if ($this->model->checkTeacherId($teacherId)) {
            $this->view->title = "Dashboard | Teacher";
            $this->view->publicCSS = array("summernote.css", "bootstrap-toggle.min.css", "bootstrap-datepicker.min.css");
            $this->view->publicJS = array("summernote.min.js", "bootstrap-toggle.min.js", "bootstrap-datepicker.min.js", "jquery.form.js", "dropzone.js");
            $this->view->css = array("admin/css/adminStyle.css");
            $this->view->js = array("org/js/autoAddress.js");
            $this->view->teacherDetail = $this->model->selectTeacherContactDetail($teacherId);
            $this->view->render("teacher/update");
        } else {
            Session::set("error", "please select a teacher");
            header("Location:" . URL . "error/fail/TeacherError");
        }  
       
    }

    function delete() {
        $this->view->title = "Dashboard | Teacher";
        $this->view->msg = "delete teacher";
        $this->view->render("teacher/index");
    }

}
