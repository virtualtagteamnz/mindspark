<?php

class Index extends Controller {

    function __construct() {
        parent::__construct();
        $this->view->css = array("index/css/index_style.css");
    }

    function index() {
        $this->view->title = 'Home';
        $this->view->msg = "Wellcome to Mindspark!";
        $this->view->scr = "";
        if (isset($_REQUEST['login'])) {
            if ($_REQUEST['login'] == "f") {
                $this->view->title = 'Failed';
                $this->view->scr = '$(".login-message").addClass("login-failed")';
                $this->view->msg = "Username or password is wrong!";
            }
        }
        $this->view->render('index/index', true);
    }

}
