<?php

class manager extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::checkLogin();
    }

    function index() {
        $this->view->title = "Dashboard | Manager";
        $this->view->publicCSS = array('bootstrap-table.min.css');
        $this->view->publicJS = array('bootstrap-table.min.js');
        $this->view->render("manager/index");
    }

    function ManagerData() {
        echo json_encode($this->model->getAllManager());
    }

    function create() {
        $this->view->title = "Dashboard | Manager";
        $this->view->orgs = $this->model->selectOrg();
        $this->view->render("manager/NewManagerForm");
    }

    function addNew() {
        $this->view->title = "Dashboard | School >> New School";
        if (isset($_POST)) {
            try {
                $form = new Form();
                $form->post('org_id')
                        ->post('username')
                        ->post('password')
                        ->post('email');
                $form->submit();
                $data = $form->fetch();
                $manager = $this->model->addNewManager($data);
                if ($manager) {
                    header("Location:" . URL . "manager/managerDetail/" . $manager);
                } else {
                    $this->view->error = 'Add Manager unsuccessfully!';
                    $this->view->render("error/fail");
                }
            } catch (Exception $exc) {
                $this->view->error = $exc;
                $this->view->render("error/fail");
            }
        }
    }

    function managerDetail($managerID = null) {
        if ($managerID == null || empty($managerID)) {
            $this->view->error = 'Please select a manager';
            header("Location:" . URL . "error/fail");
        }
        if ($this->model->checkManagerId($managerID)) {
            $this->view->title = "Dashboard | Manager";
            $this->view->publicCSS = array("summernote.css", "bootstrap-toggle.min.css", "bootstrap-datepicker.min.css");
            $this->view->publicJS = array("summernote.min.js", "bootstrap-toggle.min.js", "bootstrap-datepicker.min.js", "jquery.form.js", "dropzone.js");
            $this->view->css = array("manager/css/managerStyle.css");
            $this->view->js = array("org/js/autoAddress.js");
            $this->view->managerDetail = $this->model->selectManagerContactDetail($managerID);
            $this->view->render("manager/managerDetail");
        } else {
            Session::set("error", "please select a manager");
            header("Location:" . URL . "error/fail/managerError");
        }
    }

    function usernameAvailable() {
        if (isset($_POST)) {
            $name = $_POST['username'];
            $u = $this->model->checkUsernameAvailable($name);
            if ($u) {
                echo "no";
            } else {
                echo "yes";
            }
        }
    }

    function emailAvailable() {
        if (isset($_POST)) {
            $email = $_POST['email'];
            $e = $this->model->checkEmailAvailable($email);
            if ($e) {
                echo "no";
            } else {
                echo "yes";
            }
        }
    }

    function imageUpload() {
        if (!empty($_FILES) && !empty($_POST)) {
            $managerId = $_POST['userId'];
            $role = $_POST['role'];
            $org = $_POST['org_id'];
            $contact_id = $_POST['contact_id'];
            $tempFile = $_FILES['file']['tmp_name'];
            if (!file_exists(DOCROOT . "upload/images/user/")) {
                mkdir(DOCROOT . "upload/images/user/", 0755, true);
            } else {
                chmod(DOCROOT . "upload/images/user/", 0755);
            }
            $type = $_FILES['file']['type'];
            $type = split("/", $type);
            $type = $type[count($type) - 1];

            $targetPath = DOCROOT . "upload/images/user/";

            $fileName = "user_" . $managerId . "." . $type;

            $targetFile = $targetPath . $fileName;

            $isUploaded = move_uploaded_file($tempFile, $targetFile);
            if ($isUploaded) {
                $data = array("id" => $managerId,
                    "role" => $role,
                    "org_id" => $org,
                    "contact_id" => $contact_id,
                    "type" => $type,
                    "path" => $targetPath,
                    "filename" => $fileName,
                    "is_profile_picture" => true,
                    "url" => URL . "upload/images/user/" . $fileName);
                $image = $this->model->savePorfileImage($data);

                if ($image) {
                    $responseText = array("status" => 1, "url" => $image->url);
                } else {
                    $responseText = array("status" => 0, "url" => "http://my.mindsparks.co.nz/public/images/default-user-image.png");
                }
                echo json_encode($responseText);
            } else {
                echo "error";
            }
        }
    }

    function managerDetailForm($managerID = null) {
        if ($managerID != null) {
            if (isset($_POST)) {
                $form = new Form();
                $form->post('manager_id')
                        ->post('contact_id')
                        ->post('firstname')
                        ->post('middlename')
                        ->post('lastname')
                        ->post('dob')
                        ->post('gender')
                        ->post('email')
                        ->post('mobile')
                        ->post('org_name')
                        ->post('org_id')
                        ->post('full_address')
                        ->post('street_number')
                        ->post('address')
                        ->post('suburb')
                        ->post('city')
                        ->post('postcode')
                        ->post('isActive');
                $form->submit();
                $data = $form->fetch();
                $isAdd = $this->model->updateManagerDetail($data);
                if ($isAdd) {
                    header("Location:" . URL . "manager");
                } else {
                    Session::set("error", "add manager detail failed");
                    header("Location:" . URL . "error/fail/managerError");
                }
            } else {
                Session::set("error", "no manager data");
                header("Location:" . URL . "error/fail/managerError");
            }
        } else {
            Session::set("error", "no manager select");
            header("Location:" . URL . "error/fail/managerError");
        }
    }

    function update($manager_id) {
        if ($manager_id == null || empty($manager_id)) {
            $this->view->error = 'Please select a manager';
            header("Location:" . URL . "error/fail");
        }
        if ($this->model->checkManagerId($manager_id)) {
            $this->view->title = "Dashboard | Manager";
            $this->view->publicCSS = array("summernote.css", "bootstrap-toggle.min.css", "bootstrap-datepicker.min.css");
            $this->view->publicJS = array("summernote.min.js", "bootstrap-toggle.min.js", "bootstrap-datepicker.min.js", "jquery.form.js", "dropzone.js");
            $this->view->css = array("manager/css/managerStyle.css");
            $this->view->js = array("org/js/autoAddress.js");
            $this->view->managerDetail = $this->model->selectManagerContactDetail($manager_id);
            $this->view->render("manager/update");
        } else {
            Session::set("error", "please select a manager");
            header("Location:" . URL . "error/fail/managerError");
        }  
    }

    function delete() {
        $this->view->title = "Dashboard | Manager";
        $this->view->msg = "delete Manager";
        $this->view->render("manager/index");
    }

}
