<?php

class admin extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::checkLogin();
    }

    function index() {
        $this->view->title = "Dashboard | Admin";
        $this->view->publicCSS = array('bootstrap-table.min.css');
        $this->view->publicJS = array('bootstrap-table.min.js');
        $this->view->render("admin/index");
    }

    function view($adminId = null) {
        if ($adminId == null) {
            $this->view->error = 'Please select a admin';
            header("Location:" . URL . "error/fail");
        }
        $this->view->title = "Dashboard | Admin";
        $this->view->publicCSS = array('bootstrap-table.min.css');
        $this->view->publicJS = array('bootstrap-table.min.js');
        $this->view->admin = $this->model->getSchoolByAdmin($adminId);
        $this->view->render("admin/view");
    }

    function AdminData($adminId = null) {
        echo json_encode($this->model->getAllAdmin($adminId = null));
    }

    function create() {
        $this->view->title = "Dashboard | Admin";
        $this->view->orgs = $this->model->selectOrg();
        $this->view->render("admin/NewAdminForm");
    }

    function orgAvailable() {
        if (isset($_POST)) {
            $id = $_POST['id'];
            $school = $this->model->checkOrgSchool($id);
            if ($school) {
                $ava = array("ok" => 1, "data" => $school);
            } else {
                $ava = array("ok" => 0, "data" => $school);
            }
            echo json_encode($ava);
        }
    }

    function schoolAvailable() {
        if (isset($_POST)) {
            $id = $_POST['id'];
            $s = $this->model->checkSchoolRole($id);
            if ($s) {
                echo "yes";
            } else {
                echo "no";
            }
        }
    }

    function usernameAvailable() {
        if (isset($_POST)) {
            $name = $_POST['username'];
            $u = $this->model->checkUsernameAvailable($name);
            if ($u) {
                echo "no";
            } else {
                echo "yes";
            }
        }
    }

    function emailAvailable() {
        if (isset($_POST)) {
            $email = $_POST['email'];
            $e = $this->model->checkEmailAvailable($email);
            if ($e) {
                echo "no";
            } else {
                echo "yes";
            }
        }
    }

    function addNew() {
        $this->view->title = "Dashboard | Admin >> New Admin";
        if (isset($_POST)) {
            try {
                $form = new Form();
                $form->post('org_id')
                        ->post('school_id_value')
                        ->post('username')
                        ->post('password')
                        ->post('email');
                $form->submit();
                $data = $form->fetch();
                $admin = $this->model->addNewAdmin($data);
                if ($admin) {
                    header("Location:" . URL . "admin/adminDetail/" . $admin);
                } else {
                    $this->view->error = 'Add admin unsuccessfully!';
                    $this->view->render("error/fail");
                }
            } catch (Exception $exc) {
                $this->view->error = $exc;
                $this->view->render("error/fail");
            }
        }
    }

    function adminDetail($adminId = null) {
        if ($adminId == null || empty($adminId)) {
            $this->view->error = 'Please select a admin';
            header("Location:" . URL . "error/fail");
        }
        if ($this->model->checkAdminId($adminId)) {
            $this->view->title = "Dashboard | Admin";
            $this->view->publicCSS = array("summernote.css", "bootstrap-toggle.min.css", "bootstrap-datepicker.min.css");
            $this->view->publicJS = array("summernote.min.js", "bootstrap-toggle.min.js", "bootstrap-datepicker.min.js", "jquery.form.js", "dropzone.js");
            $this->view->css = array("admin/css/adminStyle.css");
            $this->view->js = array("org/js/autoAddress.js");
            $this->view->adminDetail = $this->model->selectAdminContactDetail($adminId);
            $this->view->render("admin/adminDetail");
        } else {
            Session::set("error", "please select a admin");
            header("Location:" . URL . "error/fail/AdminError");
        }
    }

    function imageUpload() {
        if (!empty($_FILES) && !empty($_POST)) {
            $adminId = $_POST['userId'];
            $role = $_POST['role'];
            $org = $_POST['org_id'];
            $contact_id = $_POST['contact_id'];
            $tempFile = $_FILES['file']['tmp_name'];
            if (!file_exists(DOCROOT . "upload/images/user/")) {
                mkdir(DOCROOT . "upload/images/user/", 0755, true);
            } else {
                chmod(DOCROOT . "upload/images/user/", 0755);
            }
            $type = $_FILES['file']['type'];
            $type = split("/", $type);
            $type = $type[count($type) - 1];

            $targetPath = DOCROOT . "upload/images/user/";

            $fileName = "user_" . $adminId . "." . $type;

            $targetFile = $targetPath . $fileName;

            $isUploaded = move_uploaded_file($tempFile, $targetFile);
            if ($isUploaded) {
                $data = array("id" => $adminId,
                    "role" => $role,
                    "org_id" => $org,
                    "contact_id" => $contact_id,
                    "type" => $type,
                    "path" => $targetPath,
                    "filename" => $fileName,
                    "is_profile_picture" => true,
                    "url" => URL . "upload/images/user/" . $fileName);
                $image = $this->model->savePorfileImage($data);

                if ($image) {
                    $responseText = array("status" => 1, "url" => $image->url);
                } else {
                    $responseText = array("status" => 0, "url" => "http://my.mindsparks.co.nz/public/images/default-user-image.png");
                }
                echo json_encode($responseText);
            } else {
                echo "error";
            }
        }
    }

    function adminDetailForm($adminId = null) {
        if ($adminId != null) {
            if (isset($_POST)) {
                $form = new Form();
                $form->post('adminId')
                        ->post('contact_id')
                        ->post('firstname')
                        ->post('middlename')
                        ->post('lastname')
                        ->post('dob')
                        ->post('gender')
                        ->post('email')
                        ->post('mobile')
                        ->post('org_name')
                        ->post('school_name')
                        ->post('full_address')
                        ->post('street_number')
                        ->post('address')
                        ->post('suburb')
                        ->post('city')
                        ->post('postcode')
                        ->post('isActive');
                $form->submit();
                $data = $form->fetch();
                $isAdd = $this->model->updateAdminDetail($data);
                if ($isAdd) {
                    header("Location:" . URL . "admin");
                } else {
                    Session::set("error", "add admin detail failed");
                    header("Location:" . URL . "error/fail/adminError");
                }
            } else {
                Session::set("error", "no admin data");
                header("Location:" . URL . "error/fail/adminError");
            }
        } else {
            Session::set("error", "no Admin select");
            header("Location:" . URL . "error/fail/adminError");
        }
    }

    function update($adminId) {

        if ($adminId == null || empty($adminId)) {
            $this->view->error = 'Please select a admin';
            header("Location:" . URL . "error/fail");
        }
        if ($this->model->checkAdminId($adminId)) {
            $this->view->title = "Dashboard | Admin";
            $this->view->publicCSS = array("summernote.css", "bootstrap-toggle.min.css", "bootstrap-datepicker.min.css");
            $this->view->publicJS = array("summernote.min.js", "bootstrap-toggle.min.js", "bootstrap-datepicker.min.js", "jquery.form.js", "dropzone.js");
            $this->view->css = array("admin/css/adminStyle.css");
            $this->view->js = array("org/js/autoAddress.js");
            $this->view->adminDetail = $this->model->selectAdminContactDetail($adminId);
            $this->view->render("admin/update");
        } else {
            Session::set("error", "please select a admin");
            header("Location:" . URL . "error/fail/AdminError");
        }
    }

    function delete() {
        echo 'delete ' . $id;
    }

}
