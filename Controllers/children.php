<?php

class children extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::checkLogin();
    }

    function index() {
        $this->view->title = "Dashboard | Children";
        $this->view->msg = "welcome Children";
        $this->view->render("children/index");
    }

    function create() {
        $this->view->title = "Dashboard | Children";
        $this->view->msg = "create Children";
        $this->view->render("children/index");
    }

    function update() {
        $this->view->title = "Dashboard | Children";
        $this->view->msg = "update Children";
        $this->view->render("children/index");
    }

    function delete() {
        $this->view->title = "Dashboard | Children";
        $this->view->msg = "delete Children";
        $this->view->render("children/index");
    }

}
