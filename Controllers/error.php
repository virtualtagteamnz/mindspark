<?php

class Error extends Controller {

    function __construct() {
        parent::__construct();
        Auth::checkLogin();
        $this->view->css = array();
        $this->view->js = array();
    }

    function index() {
        $this->view->title = '404 Error';
        $this->view->msg = 'This page doesnt exist';
        $this->view->render('error/index', TRUE);
    }

    function fail($error = NULL) {
        $this->view->title = 'Error';
        if ($error == null) {
            $this->view->error = 'This page doesnt exist';
        } else {
            $this->view->title = $error;
            $this->view->error = Session::get("error");
        }

        $this->view->render('error/fail');
    }

}
