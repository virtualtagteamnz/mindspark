<?php

class manager_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    function selectOrg() {
        return $this->db->select("o.id, o.name")
                        ->from("org o")
                        ->where(array("o.isActive" => 1))
                        ->ObjectAllResults();
    }

    function checkUsernameAvailable($username) {
        return $this->db->select("*")
                        ->from("user")
                        ->where(array("username" => $username))
                        ->ObjectAllResults();
    }

    function checkEmailAvailable($email) {
        return $this->db->select("*")
                        ->from("contact")
                        ->where(array("email" => $email))
                        ->ObjectAllResults();
    }

    function addNewManager($managerData) {
        if (empty($managerData)) {
            return false;
        }

        $this->db->insert('user')
                ->data(array("username" => $managerData['username'],
                    "email" => $managerData['email'],
                    "password" => $managerData['password'],
                    "isActive" => 0))
                ->now();
        $newManager = $this->db->select('id')
                ->from('user')
                ->where(array("username" => $managerData['username'],
                    "password" => $managerData['password']))
                ->ObjectResult();
        $user = Session::get('uid');
        $this->db->insert('contact')
                ->data(array('title' => 'Manager',
                    'email' => $managerData['email'],
                    'create_by' => $user,
                    'profile_picture' => 2
                ))
                ->now();
        $contact = $this->db->select("id")
                ->from("contact")
                ->where(array("title" => "Manager", 'email' => $managerData['email']))
                ->ObjectResult();

        $updateUser = $this->db->update('user')
                ->data(array("contact_id" => $contact->id))
                ->where(array("id" => $newManager->id))
                ->now();

        $org_user = $this->db->insert("org_user")
                ->data(array("org_id" => $managerData['org_id'], "user_id" => $newManager->id))
                ->now();
        if (!empty($newManager)) {
            $this->db->insert('user_role')
                    ->data(array('user_id' => $newManager->id, 'role_id' => 3))
                    ->now();
            return $newManager->id;
        } else {
            return FALSE;
        }
    }

    function checkManagerId($managerId) {
        $manager = $this->db->select("*")
                ->from('user')
                ->where(array('id' => $managerId))
                ->ObjectResult();
        if (empty($manager)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function selectManagerContactDetail($managerId) {
        return $this->db->select(array("u.id AS manager_id",
                            "u.isActive",
                            "m.url AS image",
                            "o.id AS org_id",
                            "o.name AS org_name",
                            "c.*"
                        ))
                        ->from('user u')
                        ->join('contact c', array("u.contact_id = c.id"))
                        ->join('media m', array("c.profile_picture = m.id"))
                        ->join('org_user ou', array("u.id = ou.user_id"))
                        ->join('org o', array("o.id = ou.org_id"))
                        ->where(array("u.id" => $managerId, "c.title" => "Manager"))
                        ->ObjectResult();
    }

    function getAllManager() {

        $manager = $this->db->select('u.id,c.firstname,c.lastname,c.email,u.isActive')
                ->from('user u')
                ->join("user_role ur", array("ur.user_id = u.id"))
                ->join("role r", array("r.id = ur.role_id"))
                ->join("contact c", array("u.contact_id = c.id"))
                ->join("org_user ou", array("u.id = ou.user_id"))
                ->where(array("r.id"=>3))
                ->ObjectAllResults();
        return $manager;
    }

    function savePorfileImage($imageData) {
        if (isset($imageData)) {
            $image = $this->db->select("id, url")
                    ->from("media")
                    ->where(array("table_from" => "user",
                        "media_id" => $imageData['id'],
                        "is_profile_picture" => 1,
                        "org_id" => $imageData['org_id']))
                    ->ObjectResult();
            if ($image) {
                $this->db->update('media')
                        ->data(array(
                            "file_name" => $imageData['filename'],
                            "file_path" => $imageData['path'],
                            "url" => $imageData['url'],
                            "media_type" => $imageData['type'],
                            "is_profile_picture" => 1,
                        ))
                        ->where(array("media_id" => $imageData['id'], "table_from" => "user", "org_id" => $imageData['org_id']))
                        ->now();
            } else {
                $this->db->insert('media')
                        ->data(array("table_from" => "user",
                            "media_id" => $imageData['id'],
                            "file_name" => $imageData['filename'],
                            "file_path" => $imageData['path'],
                            "url" => $imageData['url'],
                            "media_type" => $imageData['type'],
                            "is_profile_picture" => 1,
                            "org_id" => $imageData['org_id']))
                        ->now();
            }

            $image = $this->db->select("id, url")
                    ->from("media")
                    ->where(array("table_from" => "user",
                        "media_id" => $imageData['id'],
                        "file_name" => $imageData['filename'],
                        "file_path" => $imageData['path'],
                        "url" => $imageData['url'],
                        "media_type" => $imageData['type'],
                        "is_profile_picture" => 1,
                        "org_id" => $imageData['org_id']))
                    ->ObjectResult();
            $this->db->update("contact")
                    ->data(array("profile_picture" => $image->id))
                    ->where(array("id" => $imageData['contact_id']))
                    ->now();
            return $image;
        }
    }

    function updateManagerDetail($managerData) {
        if (empty($managerData)) {
            return FALSE;
        }
        $date = new DateTime('now');
        $dt = $date->format('Y-m-d H:i:s');
        $user = Session::get('uid');

        if ($managerData['isActive'] == NULL) {
            $isActive = 0;
        } else {
            $isActive = 1;
        }
        $updateManager = $this->db->update('user')
                ->data(array("isActive" => $isActive))
                ->where(array("id" => $managerData['manager_id']))
                ->now();

        $updateManagerContact = $this->db->update('contact')
                ->data(array(
                    "title" => "Manager",
                    "firstname" => $managerData['firstname'],
                    "middlename" => $managerData['middlename'],
                    "lastname" => $managerData['lastname'],
                    "dob" => $managerData['dob'],
                    "gender" => $managerData['gender'],
                    "mobile" => $managerData['mobile'],
                    "email" => $managerData['email'],
                    "full_address" => $managerData['full_address'],
                    "street_number" => $managerData['street_number'],
                    "address" => $managerData['address'],
                    "suburb" => $managerData['suburb'],
                    "city" => $managerData['city'],
                    "postcode" => $managerData['postcode'],
                    "country" => "New Zealand",
                    "update_on" => $dt,
                    "update_by" => $user
                ))->where(array("id" => $managerData['contact_id']))
                ->now();
        return $updateManagerContact;
    }

}
