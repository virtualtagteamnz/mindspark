<?php

class teacher_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    function selectOrg() {
        return $this->db->select("o.id, o.name")
                        ->from("org o")
                        ->where(array("o.isActive" => 1))
                        ->ObjectAllResults();
    }

    function addNewTeacher($teacherData) {
        if (empty($teacherData)) {
            return false;
        }
        $this->db->insert('user')
                ->data(array("username" => $teacherData['username'],
                    "email" => $teacherData['email'],
                    "password" => $teacherData['password'],
                    "school_id" => $teacherData['school_id_value'],
                    "isActive" => 0))
                ->now();
        $newTeacher = $this->db->select('id')
                ->from('user')
                ->where(array("username" => $teacherData['username'],
                    "password" => $teacherData['password'],
                    "school_id" => $teacherData['school_id_value']))
                ->ObjectResult();
        $user = Session::get('uid');
        $this->db->insert('contact')
                ->data(array('title' => 'Teacher',
                    'email' => $teacherData['email'],
                    'create_by' => $user,
                    'profile_picture' => 2
                ))
                ->now();
        $contact = $this->db->select("id")
                ->from("contact")
                ->where(array("title" => "Teacher", 'email' => $teacherData['email']))
                ->ObjectResult();
        $updateUser = $this->db->update('user')
                ->data(array("contact_id" => $contact->id))
                ->where(array("id" => $newTeacher->id))
                ->now();
        $org_user = $this->db->insert("org_user")
                ->data(array("org_id" => $teacherData['org_id'], "user_id" => $newTeacher->id))
                ->now();
        if (!empty($newTeacher)) {
            $this->db->insert('user_role')
                    ->data(array('user_id' => $newTeacher->id, 'role_id' => 4))
                    ->now();
            return $newTeacher->id;
        } else {
            return FALSE;
        }
    }

    function checkTeacherId($teacherId) {
        $teacher = $this->db->select("*")
                ->from('user')
                ->where(array('id' => $teacherId))
                ->ObjectResult();
        if (empty($teacher)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function selectTeacherContactDetail($teacherId) {
        return $this->db->select(array("u.id AS teacherId",
                            "u.isActive",
                            "m.url AS image",
                            "s.id AS schoolId",
                            "s.name AS schoolName",
                            "o.id AS orgId",
                            "o.name AS orgName",
                            "c.*"
                        ))
                        ->from('user u')
                        ->join('contact c', array("u.contact_id = c.id"))
                        ->join('media m', array("c.profile_picture = m.id"))
                        ->join('school s', array("u.school_id=s.id"))
                        ->join('org o', array("s.org_id = o.id"))
                        ->where(array("u.id" => $teacherId, "c.title" => "Teacher"))
                        ->ObjectResult();
    }

    function updateTeacherDetail($teacherData) {
        if (empty($teacherData)) {
            return FALSE;
        }
        $date = new DateTime('now');
        $dt = $date->format('Y-m-d H:i:s');
        $user = Session::get('uid');
        if ($teacherData['isActive'] == NULL) {
            $isActive = 0;
        } else {
            $isActive = 1;
        }
        $updateTeacher = $this->db->update('user')
                ->data(array("isActive" => $isActive))
                ->where(array("id" => $teacherData['userId']))
                ->now();
        $updateTeacherContact = $this->db->update('contact')
                ->data(array(
                    "title" => "Teacher",
                    "firstname" => $teacherData['firstname'],
                    "middlename" => $teacherData['middlename'],
                    "lastname" => $teacherData['lastname'],
                    "dob" => $teacherData['dob'],
                    "gender" => $teacherData['gender'],
                    "mobile" => $teacherData['mobile'],
                    "email" => $teacherData['email'],
                    "full_address" => $teacherData['full_address'],
                    "street_number" => $teacherData['street_number'],
                    "address" => $teacherData['address'],
                    "suburb" => $teacherData['suburb'],
                    "city" => $teacherData['city'],
                    "postcode" => $teacherData['postcode'],
                    "country" => "New Zealand",
                    "update_on" => $dt,
                    "update_by" => $user
                ))->where(array("id" => $teacherData['contact_id']))
                ->now();
        return $updateTeacherContact;
    }

}
