<?php

class superadmin_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    function getOrgList() {
        $superAdmin = Session::get("uid");
        $org_super = $this->db->select("org.id, org.name,org_user.id AS ouId")
                ->from("org")
                ->join("org_user", array("org_user.org_id = org.id"))
                ->where(array("isActive" => 1, "user_id" => $superAdmin))
                ->ObjectAllResults();
        $org = $this->db->select("o.id, o.name,sc.introduction")
                ->from("org o")
                ->join("site_contact sc", array("o.site_contact_id = sc.id"))
                ->where(array("isActive" => 1))
                ->ObjectAllResults();
        for ($index = 0; $index < count($org); $index++) {
            $isExist = false;
            for ($i = 0; $i < count($org_super); $i++) {
                if ($org[$index]->id == $org_super[$i]->id) {
                    $isExist = true;
                }
            }
            if ($isExist) {
                $org[$index]->state = true; 
            } else {
                $org[$index]->state = false;
            }
        }
        return $org;
    }

    function updateOrgUser($toDelete, $toAdd) {
        $isDelete = $isAdd = true;
        if (count($toDelete) > 0) {
            for ($index = 0; $index < count($toDelete); $index++) {
                $orgId = $toDelete[$index]->id;
                $isDelete = $this->db->delete("org_user")
                        ->where(array("org_id" => $orgId, "user_id" => 1))
                        ->now();
            }
        }
        if (count($toAdd) > 0) {
            for ($index = 0; $index < count($toAdd); $index++) {
                $orgId = $toAdd[$index]->id;
                $isAdd = $this->db->insert("org_user")
                        ->data(array("org_id" => $orgId, "user_id" => 1))
                        ->now();
            }
        }
        return $isDelete && $toAdd;
    }

}
