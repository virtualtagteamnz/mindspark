<?php

class org_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    function createNewOrg($orgName) {
        //insert into org to create org index
        $addOrg = $this->db->insert('org')
                ->data(array(
                    'name' => $orgName['name'],
                    'isActive' => 0))
                ->now();
        $Org = $this->db->select(array("id"))
                ->from('org')
                ->where(array('name' => $orgName['name']))
                ->ObjectResult();

        //insert into site_contact to create contact info
        $user = Session::get('uid');
        $addSiteContact = $this->db->insert('site_contact')
                ->data(array(
                    'site_type' => 'org',
                    'name' => $orgName['name'],
                    'create_by' => $user
                ))
                ->now();

        $siteContact = $this->db->select(array("id"))
                ->from("site_contact")
                ->where(array('name' => $orgName['name']))
                ->ObjectResult();

        //ensure site_contact_id link to site_contact
        $updateOrg = $this->db->update('org')
                ->data(array('site_contact_id' => $siteContact->id))
                ->where(array("id" => $Org->id, "name" => $orgName['name']))
                ->now();

        return $Org = $this->db->select(array("id", "name", "site_contact_id"))
                ->from('org')
                ->where(array('name' => $orgName['name']))
                ->ObjectResult();
    }

    function checkOrgName($orgName) {
        $org = $this->db->select("*")
                ->from('site_contact')
                ->where(array('name' => $orgName))
                ->ObjectResult();
        return $org;
    }

    function checkOrgId($orgId) {
        $org = $this->db->select("*")
                ->from('org')
                ->where(array('id' => $orgId))
                ->ObjectResult();
        if (empty($org)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function updateOrgDetail($orgData) {
        if (empty($orgData)) {
            return FALSE;
        }
        $date = new DateTime('now');
        $dt = $date->format('Y-m-d H:i:s');
        $user = Session::get('uid');
        if ($orgData['isActive'] == NULL) {
            $isActive = 0;
        } else {
            $isActive = 1;
        }
        $this->db->update('org')
                ->data(array(
                    "isActive" => $isActive,
                    "name" => $orgData['name']))
                ->where(array("id" => $orgData['id']))
                ->now();
        $update = $this->db->update('site_contact')
                ->data(array(
                    "site_type" => "org",
                    "street_number" => $orgData['street_number'],
                    "address" => $orgData['address'],
                    "suburb" => $orgData['suburb'],
                    "city" => $orgData['city'],
                    "full_address" => $orgData['full_address'],
                    "mobile" => $orgData['mobile'],
                    "fax" => $orgData['fax'],
                    "landline" => $orgData['landline'],
                    "postcode" => $orgData['postcode'],
                    "introduction" => $orgData['introduction'],
                    "description" => $orgData['description'],
                    "update_on" => $dt,
                    "update_by" => $user
                ))
                ->where(array("id" => $orgData['site_contact_id']))
                ->now();
        return TRUE;
    }

    function selectOrgContactDetail($orgId) {
        return $this->db->select(array("o.id AS orgId", "o.site_contact_id", "o.isActive", "sc.*"))
                        ->from('site_contact sc')
                        ->join('org o', array("o.site_contact_id = sc.id"))
                        ->where(array("o.id" => $orgId))
                        ->ObjectResult();
    }

    function update($orgId) {
        return $this->db->select()
                        ->from('site_contact')
                        ->where(array("id" => $orgId))
                        ->ObjectResult();
    }

    function getAllOrg() {
        $org = $this->db->select('o.id, o.name, o.isActive, sc.full_address')
                ->from('org o')
                ->join("site_contact sc", array("o.site_contact_id = sc.id"))
                ->ObjectAllResults();
        return $org;
    }

    function getAllSchool($orgId) {
        $schools = $this->db->select(array("s.id","s.name","s.isActive","sc.address","sc.suburb","sc.city", "sc.full_address", "sc.introduction"))
                ->from('school s')
                ->join("site_contact sc", array("s.site_contact_id = sc.id"))
                ->where(array("org_id" => $orgId))
                ->ObjectAllResults();
        return $schools;
    }
    function getAllManager($orgId) {
        $manager = $this->db->select('u.id,CONCAT_WS(" ", c.firstname , c.lastname) AS full_name,u.isActive')
                ->from('user u')
                ->join("contact c", array("u.contact_id = c.id"))
                ->join("org_user ou", array("u.id = ou.user_id"))
                ->where(array("ou.org_id" => $orgId,"c.title"=>"Manager"))
                ->ObjectAllResults();
        return $manager;
    }
   

}
