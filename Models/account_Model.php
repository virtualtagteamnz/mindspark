<?php

class account_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    function login($data) {
        if (isset($data)) {
            
            $user = $this->db->select(array("u.id", "r.role_name", "u.school_id"))
                    ->from("user u")
                    ->join("user_role ur", array("ur.user_id = u.id"))
                    ->join("role r", array("ur.role_id = r.id"))
                    ->where(array("u.username" => $data['username'], "u.password" => $data['password']))
                    ->ObjectResult();
            return $user;
        }
        return NULL;
    }

    function findOrg($school_id = null, $role_name = null) {
        if ($school_id == null && $role_name == "Superadmin") {
            return 0;
        }

        if ($school_id != null) {
            $org = $this->db->select("org_id")
                    ->from("school")
                    ->where(array("id"=>$school_id))
                    ->ObjectResult();
            return $org->org_id;
        }
    }

//     SELECT mi.id, mi.menu_item
//from
//    menu_item mi
//    join role_menu_item rmi on (rmi.menu_item_id = mi.id)
//    join role r on (r.id = rmi.role_id)
//    join user_role ur on (ur.role_id = r.id)
//	join user u on (u.id = ur.user_id)
//	where u.id=1;
    function userPremession($userID = null) {

        if (isset($userID) && $userID != null) {
            $menuItem = $this->db->select(array("mi.item_link", "mi.menu_item", "mi.menu"))
                    ->from("menu_item mi")
                    ->join("role_menu_item rmi", array("rmi.menu_item_id = mi.id"))
                    ->join("role r", array("r.id = rmi.role_id"))
                    ->join("user_role ur", array("ur.role_id = r.id"))
                    ->join("user u", array("u.id = ur.user_id"))
                    ->where(array("u.id" => $userID))
                    ->ObjectAllResults();

            $menu = $this->db->select(array("m.menu_text", "m.menu_link", "m.id"), TRUE)
                    ->from("menu m")
                    ->join("menu_item mi", array("m.id=mi.menu"))
                    ->join("role_menu_item rmi", array("rmi.menu_item_id = mi.id"))
                    ->join("role r", array("r.id = rmi.role_id"))
                    ->join("user_role ur", array("ur.role_id = r.id"))
                    ->join("user u", array("u.id = ur.user_id"))
                    ->where(array("u.id" => $userID))
                    ->ObjectAllResults();

            $permission = array();
            foreach ($menuItem as $item) {
                foreach ($menu as $m) {
                    if ($m->id == $item->menu) {
                        $permission[$m->menu_text][$item->menu_item] = $item->item_link;
                    }
                }
            }
            return $permission;
        }
        return NULL;
    }

}
