<?php

class admin_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    function selectOrg() {
        return $this->db->select("o.id, o.name")
                        ->from("org o")
                        ->where(array("o.isActive" => 1))
                        ->ObjectAllResults();
    }

    function checkOrgSchool($orgId) {
        return $this->db->select("s.id, s.name")
                        ->from("school s")
                        ->join("org o", array("s.org_id=o.id"))
                        ->where(array("o.isActive" => 1
                            , "o.id" => $orgId))
                        ->ObjectAllResults();
    }

    function checkUsernameAvailable($username) {
        return $this->db->select("*")
                        ->from("user")
                        ->where(array("username" => $username))
                        ->ObjectAllResults();
    }

    function checkEmailAvailable($email) {
        return $this->db->select("*")
                        ->from("contact")
                        ->where(array("email" => $email))
                        ->ObjectAllResults();
    }

    function checkSchoolRole($id) {
        return $this->db->select('*')
                        ->from('schools_role')
                        ->where(array("school_id" => $id, "role_id" => 2))
                        ->ObjectResult();
    }

    function addNewAdmin($adminData) {
        if (empty($adminData)) {
            return false;
        }

        $this->db->insert('user')
                ->data(array("username" => $adminData['username'],
                    "email" => $adminData['email'],
                    "password" => $adminData['password'],
                    "school_id" => $adminData['school_id_value'],
                    "isActive" => 0))
                ->now();
        $newAdmin = $this->db->select('id')
                ->from('user')
                ->where(array("username" => $adminData['username'],
                    "password" => $adminData['password'],
                    "school_id" => $adminData['school_id_value']))
                ->ObjectResult();
        $user = Session::get('uid');
        $this->db->insert('contact')
                ->data(array('title' => 'Admin',
                    'email' => $adminData['email'],
                    'create_by' => $user,
                    'profile_picture' => 2
                ))
                ->now();
        $contact = $this->db->select("id")
                ->from("contact")
                ->where(array("title" => "admin", 'email' => $adminData['email']))
                ->ObjectResult();

        $updateUser = $this->db->update('user')
                ->data(array("contact_id" => $contact->id))
                ->where(array("id" => $newAdmin->id))
                ->now();

        $org_user = $this->db->insert("org_user")
                ->data(array("org_id" => $adminData['org_id'], "user_id" => $newAdmin->id))
                ->now();
        if (!empty($newAdmin)) {
            $this->db->insert('user_role')
                    ->data(array('user_id' => $newAdmin->id, 'role_id' => 2))
                    ->now();
            return $newAdmin->id;
        } else {
            return FALSE;
        }
    }

    function checkAdminId($adminId) {
        $admin = $this->db->select("*")
                ->from('user')
                ->where(array('id' => $adminId))
                ->ObjectResult();
        if (empty($admin)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function selectAdminContactDetail($adminId) {
        return $this->db->select(array("u.id AS adminId",
                            "u.isActive",
                            "m.url AS image",
                            "s.id AS schoolId",
                            "s.name AS schoolName",
                            "o.id AS orgId",
                            "o.name AS orgName",
                            "c.*"
                        ))
                        ->from('user u')
                        ->join('contact c', array("u.contact_id = c.id"))
                        ->join('media m', array("c.profile_picture = m.id"))
                        ->join('school s', array("u.school_id=s.id"))
                        ->join('org o', array("s.org_id = o.id"))
                        ->where(array("u.id" => $adminId, "c.title" => "Admin"))
                        ->ObjectResult();
    }

    function getAllAdmin($schoolID = null) {
        if ($schoolID == null) {
            $admin = $this->db->select('u.id,c.firstname,c.lastname,c.email,s.name,u.isActive')
                    ->from('user u')
                    ->join("user_role ur", array("ur.user_id = u.id"))
                    ->join("role r", array("r.id = ur.role_id"))
                    ->join("contact c", array("u.contact_id = c.id"))
                    ->join("school s", array("u.school_id=s.id"))
                    ->where(array("r.id=2"))
                    ->ObjectAllResults();
        } else {
            $admin = $this->db->select('u.id,c.firstname,c.lastname,c.email,s.name,u.isActive')
                    ->from('user u')
                    ->join("user_role ur", array("ur.user_id = u.id"))
                    ->join("role r", array("r.id = ur.role_id"))
                    ->join("contact c", array("u.contact_id = c.id"))
                    ->join("school s", array("u.school_id=s.id"))
                    ->where(array("r.id=2", "s.id = " . $schoolID))
                    ->ObjectAllResults();
        }


        return $admin;
    }

    function savePorfileImage($imageData) {
        if (isset($imageData)) {
            $image = $this->db->select("id, url")
                    ->from("media")
                    ->where(array("table_from" => "user",
                        "media_id" => $imageData['id'],
                        "is_profile_picture" => 1,
                        "org_id" => $imageData['org_id']))
                    ->ObjectResult();
            if ($image) {
                $this->db->update('media')
                        ->data(array(
                            "file_name" => $imageData['filename'],
                            "file_path" => $imageData['path'],
                            "url" => $imageData['url'],
                            "media_type" => $imageData['type'],
                            "is_profile_picture" => 1,
                        ))
                        ->where(array("media_id" => $imageData['id'], "table_from" => "user", "org_id" => $imageData['org_id']))
                        ->now();
            } else {
                $this->db->insert('media')
                        ->data(array("table_from" => "user",
                            "media_id" => $imageData['id'],
                            "file_name" => $imageData['filename'],
                            "file_path" => $imageData['path'],
                            "url" => $imageData['url'],
                            "media_type" => $imageData['type'],
                            "is_profile_picture" => 1,
                            "org_id" => $imageData['org_id']))
                        ->now();
            }

            $image = $this->db->select("id, url")
                    ->from("media")
                    ->where(array("table_from" => "user",
                        "media_id" => $imageData['id'],
                        "file_name" => $imageData['filename'],
                        "file_path" => $imageData['path'],
                        "url" => $imageData['url'],
                        "media_type" => $imageData['type'],
                        "is_profile_picture" => 1,
                        "org_id" => $imageData['org_id']))
                    ->ObjectResult();
            $this->db->update("contact")
                    ->data(array("profile_picture" => $image->id))
                    ->where(array("id" => $imageData['contact_id']))
                    ->now();
            return $image;
        }
    }

    function updateAdminDetail($adminData) {
        if (empty($adminData)) {
            return FALSE;
        }
        $date = new DateTime('now');
        $dt = $date->format('Y-m-d H:i:s');
        $user = Session::get('uid');

        if ($adminData['isActive'] == NULL) {
            $isActive = 0;
        } else {
            $isActive = 1;
        }
        $updateAdmin = $this->db->update('user')
                ->data(array("isActive" => $isActive))
                ->where(array("id" => $adminData['adminId']))
                ->now();

        $updateAdminContact = $this->db->update('contact')
                ->data(array(
                    "title" => "Admin",
                    "firstname" => $adminData['firstname'],
                    "middlename" => $adminData['middlename'],
                    "lastname" => $adminData['lastname'],
                    "dob" => $adminData['dob'],
                    "gender" => $adminData['gender'],
                    "mobile" => $adminData['mobile'],
                    "email" => $adminData['email'],
                    "full_address" => $adminData['full_address'],
                    "street_number" => $adminData['street_number'],
                    "address" => $adminData['address'],
                    "suburb" => $adminData['suburb'],
                    "city" => $adminData['city'],
                    "postcode" => $adminData['postcode'],
                    "country" => "New Zealand",
                    "update_on" => $dt,
                    "update_by" => $user
                ))->where(array("id" => $adminData['contact_id']))
                ->now();
        return $updateAdminContact;
    }

    function getSchoolByAdmin($adminId) {
        return $this->db->select(array("u.id","u.school_id"))
                        ->from('user u')
                        ->join('contact c', array("u.contact_id = c.id"))
                        ->where(array('u.id' => $adminId))
                        ->ObjectResult();
    }

}
