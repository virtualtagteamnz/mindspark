<?php

class LearnService_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    function getOrgList() {
        $role = Session::get("role");
        $uid = Session::get("uid");

        return $this->db->select(array("o.id AS oId", "o.name AS oName", "sc.id AS contactId", "sc.address","sc.suburb","sc.city", "sc.full_address", "sc.introduction"))
                        ->from("site_contact sc")
                        ->join("org o", array("sc.id = o.site_contact_id"))
                        ->join("org_user ou", array("o.id = ou.org_id"))
                        ->where(array("ou.user_id" => $uid))
                        ->ObjectAllResults();
    }

}
