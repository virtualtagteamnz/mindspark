<?php

class school_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    function addNewSchool($schoolData) {
        if (empty($schoolData)) {
            return false;
        }
        $roles = array();
        $date = new DateTime('now');
        $dt = $date->format('Y-m-d H:i:s');
        $sch = $this->db->insert('school')
                ->data(array('name' => $schoolData['name'],
                    'isActive' => 0,
                    'org_id' => $schoolData['org_id'],
                    'create_time' => $dt
                ))
                ->now();
        $newSchool = $this->db->select('id')
                        ->from('school')
                        ->where(array('name' => $schoolData['name'],
                            'isActive' => 0
                        ))->ObjectResult();

        $user = Session::get('uid');
        $addSiteContact = $this->db->insert('site_contact')
                ->data(array(
                    'site_type' => 'school',
                    'name' => $schoolData['name'],
                    'create_by' => $user
                ))
                ->now();
        $siteContact = $this->db->select(array("id"))
                ->from("site_contact")
                ->where(array('name' => $schoolData['name']))
                ->ObjectResult();

        $updateSchool = $this->db->update('school')
                ->data(array('site_contact_id' => $siteContact->id))
                ->where(array("id" => $newSchool->id, "name" => $schoolData['name']))
                ->now();

        if ($schoolData['role'] === 'true') {
            for ($index = 0; $index < 4; $index++) {
                $this->db->insert('schools_role')
                        ->data(array('role_id' => ($index + 2), 'school_id' => $newSchool->id))
                        ->now();
            }
        } else {
            for ($index = 0; $index < count($schoolData['selectRole']); $index++) {
                $this->db->insert('schools_role')
                        ->data(array('role_id' => $schoolData['selectRole'][$index], 'school_id' => $newSchool->id))
                        ->now();
            }
        }
        return $Org = $this->db->select(array("id", "name", "site_contact_id"))
                ->from('school')
                ->where(array('name' => $schoolData['name']))
                ->ObjectResult();
    }

    function selectSchoolContactDetail($orgId) {
        return $this->db->select(array("s.id AS schoolId", "s.site_contact_id", "s.isActive", "sc.*"))
                        ->from('site_contact sc')
                        ->join('school s', array("s.site_contact_id = sc.id"))
                        ->where(array("s.id" => $orgId))
                        ->ObjectResult();
    }

    function schoolNameAvailable($schoolName) {
        $school = $this->db->select("name")
                ->from("site_contact")
                ->where(array("name" => $schoolName))
                ->ObjectResult();
        return $school;
    }

    function updateSchoolDetail($schoolData) {
        if (empty($schoolData)) {
            return FALSE;
        }
        $date = new DateTime('now');
        $dt = $date->format('Y-m-d H:i:s');
        $user = Session::get('uid');
        if ($schoolData['isActive'] == NULL) {
            $isActive = 0;
        } else {
            $isActive = 1;
        }

        $updateSchool = $this->db->update('school')
                ->data(array("isActive" => $isActive, "name" => $schoolData['name']))
                ->where(array("id" => $schoolData['id']))
                ->now();

        $updateSiteContact = $this->db->update('site_contact')
                ->data(array(
                    "site_type" => "school",
                    "street_number" => $schoolData['street_number'],
                    "address" => $schoolData['address'],
                    "suburb" => $schoolData['suburb'],
                    "city" => $schoolData['city'],
                    "full_address" => $schoolData['full_address'],
                    "mobile" => $schoolData['mobile'],
                    "fax" => $schoolData['fax'],
                    "landline" => $schoolData['landline'],
                    "postcode" => $schoolData['postcode'],
                    "introduction" => $schoolData['introduction'],
                    "description" => $schoolData['description'],
                    "update_on" => $dt,
                    "update_by" => $user
                ))
                ->where(array("id" => $schoolData['site_contact_id']))
                ->now();
        return TRUE;
    }

    function checkSchoolId($schoolId) {
        $school = $this->db->select("*")
                ->from('school')
                ->where(array('id' => $schoolId))
                ->ObjectResult();
        if (empty($school)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function getAllSchool() {
        $schools = $this->db->select('id,name,isActive')
                ->from('school')
                ->ObjectAllResults();
        return $schools;
    }

    function getAllAdmin($schoolId) {
        $schools = $this->db->select('u.id,CONCAT_WS(" ", c.firstname , c.lastname) AS full_name,c.email, u.isActive')
                ->from('user u')
                ->join("contact c", array("u.contact_id = c.id"))
                ->where(array("c.title" => "Admin", "u.school_id" => $schoolId))
                ->ObjectAllResults();
        return $schools;
    }

    function getAllTeacher($schoolId) {
        $teacher = $this->db->select('u.id,CONCAT_WS(" ", c.firstname , c.lastname) AS full_name,c.email, u.isActive')
                ->from('user u')
                ->join("contact c", array("u.contact_id = c.id"))
                ->where(array("c.title" => "Teacher", "u.school_id" => $schoolId))
                ->ObjectAllResults();
        return $teacher;
    }
    function getAllParent($schoolId) {
        $teacher = $this->db->select('u.id,CONCAT_WS(" ", c.firstname , c.lastname) AS full_name,c.email, u.isActive')
                ->from('user u')
                ->join("contact c", array("u.contact_id = c.id"))
                ->where(array("c.title" => "Parents", "u.school_id" => $schoolId))
                ->ObjectAllResults();
        return $teacher;
    }

    function selectOrgs() {
        return $this->db->select("id, name")
                        ->from("org")
                        ->where(array("isActive" => 1))
                        ->ObjectAllResults();
    }

}
