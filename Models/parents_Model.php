<?php

class parents_Model extends Model {

    public function __construct() {
        parent::__construct();
    }

    function selectOrg() {
        return $this->db->select("o.id, o.name")
                        ->from("org o")
                        ->where(array("o.isActive" => 1))
                        ->ObjectAllResults();
    }

    function addNewParent($parentData) {
        if (empty($parentData)) {
            return false;
        }
        $this->db->insert('user')
                ->data(array("username" => $parentData['username'],
                    "email" => $parentData['email'],
                    "password" => $parentData['password'],
                    "school_id" => $parentData['school_id_value'],
                    "isActive" => 0))
                ->now();
        $newParent = $this->db->select('id')
                ->from('user')
                ->where(array("username" => $parentData['username'],
                    "password" => $parentData['password'],
                    "school_id" => $parentData['school_id_value']))
                ->ObjectResult();
        $user = Session::get('uid');
        $this->db->insert('contact')
                ->data(array('title' => 'Parents',
                    'email' => $parentData['email'],
                    'create_by' => $user,
                    'profile_picture' => 2
                ))
                ->now();
        $contact = $this->db->select("id")
                ->from("contact")
                ->where(array("title" => "Parents", 'email' => $parentData['email']))
                ->ObjectResult();
        $updateUser = $this->db->update('user')
                ->data(array("contact_id" => $contact->id))
                ->where(array("id" => $newParent->id))
                ->now();
        $org_user = $this->db->insert("org_user")
                ->data(array("org_id" => $parentData['org_id'], "user_id" => $newParent->id))
                ->now();
        if (!empty($newParent)) {
            $this->db->insert('user_role')
                    ->data(array('user_id' => $newParent->id, 'role_id' => 5))
                    ->now();
            return $newParent->id;
        } else {
            return FALSE;
        }
    }

    function checkParentId($parentId) {
        $parent = $this->db->select("*")
                ->from('user')
                ->where(array('id' => $parentId))
                ->ObjectResult();
        if (empty($parent)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function selectParentContactDetail($parentId) {
        return $this->db->select(array("u.id AS parentId",
                            "u.isActive",
                            "m.url AS image",
                            "s.id AS schoolId",
                            "s.name AS schoolName",
                            "o.id AS orgId",
                            "o.name AS orgName",
                            "c.*"
                        ))
                        ->from('user u')
                        ->join('contact c', array("u.contact_id = c.id"))
                        ->join('media m', array("c.profile_picture = m.id"))
                        ->join('school s', array("u.school_id=s.id"))
                        ->join('org o', array("s.org_id = o.id"))
                        ->where(array("u.id" => $parentId, "c.title" => "Parents"))
                        ->ObjectResult();
    }

    function updateParentDetail($parentData) {
        if (empty($parentData)) {
            return FALSE;
        }
        $date = new DateTime('now');
        $dt = $date->format('Y-m-d H:i:s');
        $user = Session::get('uid');
        if ($parentData['isActive'] == NULL) {
            $isActive = 0;
        } else {
            $isActive = 1;
        }
        $updateAdmin = $this->db->update('user')
                ->data(array("isActive" => $isActive))
                ->where(array("id" => $parentData['userId']))
                ->now();
        $updateParentContact = $this->db->update('contact')
                ->data(array(
                    "title" => "Parents",
                    "firstname" => $parentData['firstname'],
                    "middlename" => $parentData['middlename'],
                    "lastname" => $parentData['lastname'],
                    "dob" => $parentData['dob'],
                    "gender" => $parentData['gender'],
                    "mobile" => $parentData['mobile'],
                    "email" => $parentData['email'],
                    "full_address" => $parentData['full_address'],
                    "street_number" => $parentData['street_number'],
                    "address" => $parentData['address'],
                    "suburb" => $parentData['suburb'],
                    "city" => $parentData['city'],
                    "postcode" => $parentData['postcode'],
                    "country" => "New Zealand",
                    "update_on" => $dt,
                    "update_by" => $user
                ))->where(array("id" => $parentData['contact_id']))
                ->now();
        return $updateParentContact;
    }

}
