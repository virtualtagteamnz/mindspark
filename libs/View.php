<?php

class View {

    public function __construct() {
        
    }

    public function render($viewFile, $noInclude = false) {
        if ($noInclude) {
            require 'Views/header/head.php';
            require 'Views/' . $viewFile . '.php';
        } else {
            require 'Views/header/head.php';
            require 'Views/header/header.php';
            require 'Views/' . $viewFile . '.php';
            require 'Views/footer/footer.php';
        }
    }
}
