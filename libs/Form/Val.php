<?php

class Val {

    public function __construct() {
        
    }

    public function minlength($data, $arg) {
        if (strlen($data) < $arg) {
            return "Your string can only be $arg long";
        }
    }

    public function maxlength($data, $arg) {
        if (strlen($data) > $arg) {
            return "Your string can only be $arg long!";
        }
    }

    public function digit($data) {
        if (ctype_digit($data) == false) {
            return "Your string must be a digit!";
        }
    }

    public function emailFormat($data) {
        if (!filter_var($data, FILTER_VALIDATE_EMAIL)) {
            return "Your email must be right format!";
        }
    }

    public function confirm($password, $repeatPassword) {
        if ($password != $repeatPassword) {
            return "Please repeat same password!";
        }
    }

    public function __call($name, $arguments) {
        throw new Exception("$name does not exist inside of: " . __CLASS__);
    }

}
